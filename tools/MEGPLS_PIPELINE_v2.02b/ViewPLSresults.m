function varargout = ViewPLSresults(varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Visualization HUB for viewing PLS 4D BRIK files. %
% Last modified: Feb. 26, 2014                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


% Last Modified by GUIDE v2.5 20-Sep-2014 05:49:56

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ViewPLSresults_OpeningFcn, ...
                   'gui_OutputFcn',  @ViewPLSresults_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


%--- Executes just before ViewPLSresults is made visible. ---%
%------------------------------------------------------------%
function ViewPLSresults_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ViewPLSresults (see VARARGIN)

% Choose default command line output for ViewPLSresults
handles.output = hObject;


% Make sure toolbox paths are added:
[PipelineDir, ~, ~] = fileparts(which('ViewPLSresults.m'));

addpath(genpath(PipelineDir));
rmpath([PipelineDir,'/DEFAULT_SETTINGS']);  % Make sure its calling from AnalysisID
rmpath([PipelineDir,'/TEMPORARY_FIXES']);   % Make sure its calling from FT toolbox

CheckToolboxPaths(PipelineDir);


% Initialize variables:
handles.paths.OrigAnatFile = ...
    [PipelineDir,'/MEGPLS_TOOLBOX/Masks_Templates/template_ch2+tlrc.BRIK'];
set(handles.TextboxAnatFile, 'String', handles.paths.OrigAnatFile);

handles.gui.PLSimgList      = [];
handles.paths.PLSresultsDir = [];
handles.paths.CurrentPLSimg = [];

handles.gui.GrpAvgCorrFileList       = [];
handles.paths.CurrentGrpAvgCorrFiles = [];


% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ViewPLSresults wait for user response (see UIRESUME)
% uiwait(handles.figure1);


%--- Outputs from this function are returned to the command line. ---%
%--------------------------------------------------------------------%
function varargout = ViewPLSresults_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;




%====================================%
% FUNCTIONS FOR ANATOMICAL UNDERLAY: %
%====================================%

%--- Textbox to display selected anatomical .BRIK file: ---%
%----------------------------------------------------------%
function TextboxAnatFile_Callback(hObject, eventdata, handles)
EnteredText = get(handles.TextboxAnatFile, 'String');

if ~isequal(EnteredText, handles.paths.OrigAnatFile)
    set(handles.TextboxAnatFile, 'String', handles.paths.OrigAnatFile);
    msgbox('Note: Use button to change anatomy file.');
end


%--- Executes on button press in ButtonAnatFile. ---%
%---------------------------------------------------%
function ButtonAnatFile_Callback(hObject, eventdata, handles)
[AnatPath, ~, ~]     = fileparts(handles.paths.OrigAnatFile);
[BrikFile, BrikPath] = uigetfile([AnatPath,'/*+tlrc.BRIK'], ...
    'Select anatomical underlay .BRIK file.', 'MultiSelect', 'off');

if BrikFile == 0
    return;  % If user cancels
end

% Set anatomy file:
handles.paths.OrigAnatFile = [BrikPath,BrikFile];
set(handles.TextboxAnatFile, 'String', handles.paths.OrigAnatFile)

% Save handles:
guidata(hObject, handles);




%=================================================%
% FUNCTIONS FOR PLS IMAGE & TIMESERIES SELECTION: %
%=================================================%

%--- Textbox to display selected PLS results directory: ---%
%----------------------------------------------------------%
function TextboxPLSresultsDir_Callback(hObject, eventdata, handles)
EnteredText = get(handles.TextboxPLSresultsDir, 'String');

if ~isequal(EnteredText, handles.paths.PLSresultsDir)
    set(handles.TextboxPLSresultsDir, 'String', handles.paths.PLSresultsDir);
    msgbox('Note: Use button to change PLS results directory.');
end


%--- Executes on selection change in ListboxPLSresultsFiles. ---%
%---------------------------------------------------------------%
function ListboxPLSresultsFiles_Callback(hObject, eventdata, handles)
if isempty(handles.paths.PLSresultsDir) || isempty(handles.gui.PLSimgList)
    return;
end

handles = DetectSelectedFiles(handles);
guidata(hObject, handles);


%--- Executes on selection change in ListboxGrpAvgCorrFiles. ---%
%---------------------------------------------------------------%
function ListboxGrpAvgCorrFiles_Callback(hObject, eventdata, handles)
if isempty(handles.paths.PLSresultsDir) || isempty(handles.gui.GrpAvgCorrFileList)
    return;
end

handles = DetectSelectedFiles(handles);
guidata(hObject, handles);


%--- Executes on button press in ButtonPLSresultsDir. ---%
%--------------------------------------------------------%
function ButtonPLSresultsDir_Callback(hObject, eventdata, handles)
if ~isempty(handles.paths.PLSresultsDir)
    [OneUpDir, ~, ~] = fileparts(handles.paths.PLSresultsDir);
else
    OneUpDir = [];
end

SelectedPath = uigetdir(OneUpDir, 'Select PLS results folder:');
if SelectedPath == 0
    return;  % If user cancels
end

% Get list of PLS image files & GrpAvg or Corr timeseries files:
PLSimgFiles = dir([SelectedPath,'/BSRimg_*_LV*+tlrc.BRIK']);
PLSimgFiles = [PLSimgFiles; dir([SelectedPath,'/SALimg_*_LV*+tlrc.BRIK'])];
if isempty(PLSimgFiles)
    msgbox('Error: No PLS image files found inside directory.', 'Error:')
    return;
end

TimeseriesFiles = dir([SelectedPath,'/*TS_*+tlrc.BRIK']);
if isempty(TimeseriesFiles)
    msgbox('Error: No GrpAvg / Corr timeseries files found inside directory.', 'Error:')
    return;
end

% Set current path and available files:
handles.paths.PLSresultsDir = SelectedPath;
set(handles.TextboxPLSresultsDir, 'String', handles.paths.PLSresultsDir);

handles.gui.PLSimgList         = {PLSimgFiles.name}';
handles.gui.GrpAvgCorrFileList = {TimeseriesFiles.name}';

handles = UpdatePLSimgTimeseries (handles);
handles = DetectSelectedFiles    (handles);

% Save handles:
guidata(hObject, handles);


%--- Update PLSimg & timeseries settings: ---%
%--------------------------------------------%
function OutputHandles = UpdatePLSimgTimeseries(InputHandles)
handles = InputHandles;

% Update PLSimg listbox:
set(handles.ListboxPLSresultsFiles, 'String', handles.gui.PLSimgList);

CurrentIndex = get(handles.ListboxPLSresultsFiles, 'Value');
MaxIndex     = length(handles.gui.PLSimgList);
if isempty(CurrentIndex) || CurrentIndex == 0 || CurrentIndex > MaxIndex
    set(handles.ListboxPLSresultsFiles, 'Value', MaxIndex);
end

% Update GrpAvg / Corr timeseries listbox:
set(handles.ListboxGrpAvgCorrFiles, 'String', handles.gui.GrpAvgCorrFileList);

CurrentIndex = get(handles.ListboxGrpAvgCorrFiles, 'Value');
MaxIndex     = length(handles.gui.GrpAvgCorrFileList);
if max(CurrentIndex) > MaxIndex || min(CurrentIndex) < 1 || ...
        isempty(CurrentIndex) || (length(CurrentIndex) == 1 && CurrentIndex == 0)
    set(handles.ListboxGrpAvgCorrFiles, 'Value', MaxIndex);
end

% Set output handles:
OutputHandles = handles;


%--- Acquire currently selected PLSimg & timeseries files: ---%
%-------------------------------------------------------------%
function OutputHandles = DetectSelectedFiles(InputHandles)
handles = InputHandles;

handles.paths.CurrentPLSimg          = [];
handles.paths.CurrentGrpAvgCorrFiles = [];

% Get selected PLSimg:
SelectedIndex = get(handles.ListboxPLSresultsFiles, 'Value');
SelectedFile  = handles.gui.PLSimgList{SelectedIndex};
handles.paths.CurrentPLSimg = strcat([handles.paths.PLSresultsDir,'/'], SelectedFile);

% Get selected timeseries files:
SelectedIndices = get(handles.ListboxGrpAvgCorrFiles, 'Value');
SelectedFiles   = handles.gui.GrpAvgCorrFileList(SelectedIndices);

handles.paths.CurrentGrpAvgCorrFiles = ...
    strcat([handles.paths.PLSresultsDir,'/'], SelectedFiles);

% Set output handles:
OutputHandles = handles;


%--- Executes on button press in ButtonTimeLegend. ---%
%-----------------------------------------------------%
function ButtonTimeLegend_Callback(hObject, eventdata, handles)
if isempty(handles.paths.PLSresultsDir)
    msgbox('Error: PLS results folder not selected.', 'Error:')
    return;
end

if exist([handles.paths.PLSresultsDir,'/BSRimg_TimeLegend.txt'], 'file')
    open([handles.paths.PLSresultsDir,'/BSRimg_TimeLegend.txt']);
else
    msgbox('Error: TimeLegend file missing for PLS results.', 'Error:');
    return;
end


%--- Executes on button press in ButtonShowLVPlot. ---%
%-----------------------------------------------------%
function ButtonShowLVPlot_Callback(hObject, eventdata, handles)
if isempty(handles.paths.PLSresultsDir)
    msgbox('Error: PLS results folder not selected.', 'Error:')
    return;
end

% Acquire LV figures for selection:
DetectedFigs = dir([handles.paths.PLSresultsDir,'/*_LV*.fig']);
if isempty(DetectedFigs)
    msgbox('Error: No LV figures were detected inside directory.', 'Error:');
    return;
end

% Select LV figure to open:
DetectedFigs  = {DetectedFigs.name};
SelectedIndex = listdlg('PromptString', 'Select LV figure to open:',...
    'ListSize', [300, 300], 'SelectionMode', 'single', 'ListString', DetectedFigs);

if isempty(SelectedIndex)
    return;  % If user cancels
end

SelectedFig = DetectedFigs{SelectedIndex};
open([handles.paths.PLSresultsDir,'/',SelectedFig]);




%=========================%
% FUNCTIONS FOR PLOTTING: %
%=========================%

%--- Executes on button press in ButtonView4DBSR. ---%
%----------------------------------------------------%
function ButtonView4DBSR_Callback(hObject, eventdata, handles)
if isempty(handles.paths.OrigAnatFile)
    msgbox('Error: Anatomical underlay not selected.', 'Error:');
    return;
end
if isempty(handles.paths.PLSresultsDir)
    msgbox('Error: PLS results directory not selected.', 'Error:');
    return;
end
if isempty(handles.paths.CurrentPLSimg)
    msgbox('Error: PLS image file not selected.', 'Error:');
    return;
end 
if isempty(handles.paths.CurrentGrpAvgCorrFiles)
    msgbox('Error: GrpAvg / Corr timeseries file(s) not selected.', 'Error:');
    return;
end

% Check if paths called in AFNI functions have spaces (AFNI cannot support spaces):
CheckSpaces1 = strfind(handles.paths.OrigAnatFile, ' ');
CheckSpaces2 = strfind(handles.paths.PLSresultsDir, ' ');
if ~isempty(CheckSpaces1) || ~isempty(CheckSpaces2)
    msgbox('Error: AFNI fcns cannot read folder & file paths containing spaces.', 'Error:')
    return;
end

% Update selected files:
handles = UpdatePLSimgTimeseries (handles);
handles = DetectSelectedFiles    (handles);

% Check if file is 4D:
CurrentDir = pwd;
Opt.format = 'vector';

cd(handles.paths.PLSresultsDir);
[~, PLSimg, ~, ~] = BrikLoad(handles.paths.CurrentPLSimg, Opt);
cd(CurrentDir);

if numel(size(PLSimg)) ~= 4
    msgbox('Error: Selected file is not 4D file. Use "View in AFNI" instead.');
    return;
end

% Copy anat file to PLS results directory & resample:
[~, OrigAnatName, ~] = fileparts(handles.paths.OrigAnatFile);
CopiedAnatFile = [handles.paths.PLSresultsDir,'/',OrigAnatName,'.BRIK'];

if exist(CopiedAnatFile, 'file')
    delete(CopiedAnatFile);
    delete([handles.paths.PLSresultsDir,'/',OrigAnatName,'.HEAD']);
end

system(['3dcopy ',handles.paths.OrigAnatFile,' ',CopiedAnatFile]);

ResampledAnat = MEGpipeline_AfniResampleAnat...
    (CopiedAnatFile, handles.paths.CurrentPLSimg);

if isempty(ResampledAnat)
    msgbox('Error: Failed to resampled anatomy file.', 'Error:');
    return;
end

% Run View4D_BSR:
View4D_BSR(ResampledAnat, ...
    handles.paths.CurrentPLSimg, handles.paths.CurrentGrpAvgCorrFiles);


%--- Executes on button press in ButtonViewAFNI. ---%
%---------------------------------------------------%
function ButtonViewAFNI_Callback(hObject, eventdata, handles)
if isempty(handles.paths.OrigAnatFile)
    msgbox('Error: Anatomical underlay not selected.', 'Error:');
    return;
end
if isempty(handles.paths.PLSresultsDir)
    msgbox('Error: PLS results directory not selected.', 'Error:');
    return;
end
if isempty(handles.paths.CurrentPLSimg)
    msgbox('Error: PLS image file not selected.', 'Error:');
    return;
end

% Check if paths called in AFNI functions have spaces (AFNI cannot support spaces):
CheckSpaces1 = strfind(handles.paths.OrigAnatFile, ' ');
CheckSpaces2 = strfind(handles.paths.PLSresultsDir, ' ');
if ~isempty(CheckSpaces1) || ~isempty(CheckSpaces2)
    msgbox('Error: AFNI fcns cannot read folder & file paths containing spaces.', 'Error:')
    return;
end

% Update selected files:
handles = UpdatePLSimgTimeseries (handles);
handles = DetectSelectedFiles    (handles);

% Copy anat file to PLS results directory & resample:
[~, OrigAnatName, ~] = fileparts(handles.paths.OrigAnatFile);
CopiedAnatFile = [handles.paths.PLSresultsDir,'/',OrigAnatName,'.BRIK'];

if exist(CopiedAnatFile, 'file')
    delete(CopiedAnatFile);
    delete([handles.paths.PLSresultsDir,'/',OrigAnatName,'.HEAD']);
end

system(['3dcopy ',handles.paths.OrigAnatFile,' ',CopiedAnatFile]);

ResampledAnat = MEGpipeline_AfniResampleAnat...
    (CopiedAnatFile, handles.paths.CurrentPLSimg);

if isempty(ResampledAnat)
    msgbox('Error: Failed to resampled anatomy file.', 'Error:');
    return;
end

% Prepare file calls to AFNI:
[~, AnatName, ~]    = fileparts(CopiedAnatFile);
AnatName(end-4:end) = [];  % Remove +view label

[~, PLSimgName, ~]    = fileparts(handles.paths.CurrentPLSimg);
PLSimgName(end-4:end) = [];

% Set environmental variables:
setenv('AFNI_SLAVE_FUNCTIME',    'YES');    % Locks overlay with time-index (Newer versions).
setenv('AFNI_SLAVE_BUCKETS_TOO', 'YES');    % Locks overlay with time-index (Older versions).
setenv('AFNI_SLAVE_THRTIME',     'YES');    % Locks threshold with time-index (Older versions).
setenv('AFNI_SLAVE_THROLAY',     'OLay');   % Locks threshold with time-index (Newer versions).
setenv('AFNI_LEFT_IS_LEFT',      'YES');    % Sets images to neurological view.
setenv('AFNI_THRESH_AUTO',       'NO');     % Threshold slider does not change automatically.
setenv('AFNI_THRESH_LOCK',       'VALUE');
setenv('AFNI_FLOATSCAN',         'YES');

if strcmp(getenv('AFNI_LEFT_IS_LEFT'), 'NO')
    msgbox('NOTE: "AFNI_LEFT_IS_LEFT" env. variable is now set to ''YES''.')
end

if strcmp(PLSimgName(1:7), 'SALimg_')
    CmdLine = ['afni -com '''               ...
        'OPEN_WINDOW A.axialimage;'        ...
        ' OPEN_WINDOW A.sagittalimage;'     ...
        ' OPEN_WINDOW A.coronalimage;'      ...
        ' SWITCH_UNDERLAY A.',AnatName,';'  ...  % Underlay .BRIK
        ' SWITCH_OVERLAY A.',PLSimgName,';' ...  % Overlay .BRIK
        ' SET_SUBBRICKS A 1 1 1;'           ...  % Sets .BRIK index to 1
        ' SET_THRESHOLD A.0000 0;'          ...  % Threshold to 0 & power of 0
        ' SET_FUNC_AUTORANGE A.-;'          ...  % Turns off autorange
        ' SET_FUNC_RANGE A.0000.005;'         ...  % Sets range to 0.005
        ' SET_VIEW A.tlrc;'                 ...  % Sets view to +tlrc
        ' REDISPLAY;'' &'];
    
else
    CmdLine = ['afni -com '''               ...
        'OPEN_WINDOW A.axialimage;'        ...
        ' OPEN_WINDOW A.sagittalimage;'     ...
        ' OPEN_WINDOW A.coronalimage;'      ...
        ' SWITCH_UNDERLAY A.',AnatName,';'  ...  % Underlay .BRIK
        ' SWITCH_OVERLAY A.',PLSimgName,';' ...  % Overlay .BRIK
        ' SET_SUBBRICKS A 1 1 1;'           ...  % Sets .BRIK index to 1
        ' SET_THRESHOLD A.2500 1;'          ...  % Threshold to 2.5 & power of 1
        ' SET_FUNC_AUTORANGE A.-;'          ...  % Turns off autorange
        ' SET_FUNC_RANGE A.0005;'           ...  % Sets range to 5
        ' SET_VIEW A.tlrc;'                 ...  % Sets view to +tlrc
        ' REDISPLAY;'' &'];
end
    
CurrentDir = pwd;
cd(handles.paths.PLSresultsDir);

system(CmdLine);
cd(CurrentDir);


%--- Executes on button press in ButtonBrainScores. ---%
%------------------------------------------------------%
function ButtonBrainScores_Callback(hObject, eventdata, handles)
if isempty(handles.paths.PLSresultsDir)
    msgbox('Error: PLS results directory not selected.', 'Error:');
    return;
end

% Get current BSRimg or SALimg selected:
CurrentIndex  = get(handles.ListboxPLSresultsFiles, 'Value');
CurrentPLSres = handles.gui.PLSimgList{CurrentIndex};

% Change prefix & suffix to get corresponding PLSres file:
CurrentPLSres(1:6)             = 'PLSres';
CurrentPLSres((end-13):end)    = [];
CurrentPLSres((end+1):(end+4)) = '.mat';

% Load PLS result .mat file:
if ~exist([handles.paths.PLSresultsDir,'/',CurrentPLSres], 'file')
    msgbox('Error: PLS result .mat file could not be found for selected image.')
    return;
end

PLSresult = LoadFTmat...
    ([handles.paths.PLSresultsDir,'/',CurrentPLSres], 'ViewPLSresults');

%** Old code...
% PLSresultFiles = dir([handles.paths.PLSresultsDir,'/PLSres_*.mat']);
% 
% if isempty(PLSresultFiles)
%     msgbox('Error: No PLS result .mat files found inside target directory.')
%     return;
% end
% 
% if length(PLSresultFiles) == 1
%     PLSresult = LoadFTmat...
%         ([handles.paths.PLSresultsDir,'/',PLSresultFiles(1).name], 'ViewPLSresults');
% else
%     ErrMsg = {'Error:'; '';
%         'Multiple PLS result .mat files found in the target directory.';
%         'Each PLS run should be kept in its own respective folder.'; ''};
%     msgbox(ErrMsg, 'Error:');
%     return;
% end

% Load PLSmat .mat backup:
PLSmatFile = [handles.paths.PLSresultsDir,'/Backup_PLSmat_Settings.mat'];
if ~exist(PLSmatFile, 'file')
    msgbox('Error: Backup_PLSmat_Settings.mat file not found in target directory.')
    return;
end

PLSmat = LoadFTmat(PLSmatFile, 'ViewPLSresults');

% Ask user which LV to plot:
for LV = 1:size(PLSresult.v, 2)
    LVstring{LV} = ['LV',num2str(LV)];
end

LVindex = listdlg('PromptString', 'Select LV to plot Brain Scores for:',...
    'ListSize', [300, 300], 'SelectionMode', 'single', 'ListString', LVstring);


% Reshape brain score data for LV (for bar plot):
NumCond = PLSresult.num_conditions;
NumGrps = PLSresult.num_groups; 

BrainScore = PLSresult.usc(:, LVindex);  % (Subj*Cond*Group)

GrpStartIndex = 1;

for g = 1:NumGrps
    NumGrpSubj  = PLSresult.num_subj_lst(g);        % # subj in grp
    NumGrpRows  = NumGrpSubj * NumCond;             % # total rows in grp
    GrpEndIndex = GrpStartIndex + NumGrpRows - 1;
    
    GrpBrainScore{g} = BrainScore(GrpStartIndex:GrpEndIndex);  % {g}(Subj*Cond)
    GrpStartIndex    = GrpEndIndex + 1;  % Set start index for next grp
    
    GrpBrainScore{g} = reshape(GrpBrainScore{g}, NumGrpSubj, NumCond);  % {g}(Subj, Cond)
    GrpBrainScore{g} = permute(GrpBrainScore{g}, [2 1]);                % {g}(Cond, Subj)
end

% Plot brain score for each group:  ** OR plot (Cond, Subj*Group)
% This set will be visualized with line plots (line for each cond).
for g = 1:NumGrps
    figure;
    plot(GrpBrainScore{g}');
    hold on;
    
    title(['LV',num2str(LVindex),' Brain Scores for: ',PLSmat.name.GroupID{g}]);
    
    clickableLegend(PLSmat.name.CondID)
    axis manual;
    
    set(gca, 'XLim', [1,length(PLSmat.name.SubjID{g})]);
    set(gca, 'XTick', [1:length(PLSmat.name.SubjID{g})]);
    set(gca, 'XTickLabel', PLSmat.name.SubjID{g});
    ylabel('Brain Scores:', 'Interpreter', 'none');
    xlabel('Subjects within Group:');
end

% Plot brain score for each group:  ** OR plot (Cond, Subj*Group)
% This set will be visualized with bar plots (bar for each subject).
for g = 1:NumGrps
    figure;
    bar(GrpBrainScore{g});
    hold on;
    
    title(['LV',num2str(LVindex),' Brain Scores for: ',PLSmat.name.GroupID{g}]);
    
    clickableLegend(PLSmat.name.SubjID{g});
    axis manual
    
    set(gca, 'XTickLabel', PLSmat.name.CondID);
    ylabel('Brain Scores:', 'Interpreter', 'none');
    xlabel('Conditions:', 'Interpreter', 'none');
    
    hold off;
end

% Remove ErrorLog if no problems:
if exist([pwd,'/ErrorLog_ViewPLSresults.txt'], 'file');
    LogCheck = dir('ErrorLog_ViewPLSresults.txt');
    if LogCheck.bytes == 0
        delete('ErrorLog_ViewPLSresults.txt');
    end
end 


%--- Executes on button press in ButtonSingularValues. ---%
%---------------------------------------------------------%
function ButtonSingularValues_Callback(hObject, eventdata, handles)
if isempty(handles.paths.PLSresultsDir)
    msgbox('Error: PLS results directory not selected.', 'Error:');
    return;
end

% Get current BSRimg or SALimg selected:
CurrentIndex  = get(handles.ListboxPLSresultsFiles, 'Value');
CurrentPLSres = handles.gui.PLSimgList{CurrentIndex};

% Change prefix & suffix to get corresponding PLSres file:
CurrentPLSres(1:6)             = 'PLSres';
CurrentPLSres((end-13):end)    = [];
CurrentPLSres((end+1):(end+4)) = '.mat';

% Load PLS result .mat file:
if ~exist([handles.paths.PLSresultsDir,'/',CurrentPLSres], 'file')
    msgbox('Error: PLS result .mat file could not be found for selected image.')
    return;
end

PLSresult = LoadFTmat...
    ([handles.paths.PLSresultsDir,'/',CurrentPLSres], 'ViewPLSresults');

%** Old code...
% PLSresultFiles = dir([handles.paths.PLSresultsDir,'/PLSres_*.mat']);
% 
% if isempty(PLSresultFiles)
%     msgbox('Error: No PLS result .mat files found inside target directory.')
%     return;
% end
%
% if length(PLSresultFiles) == 1
%     PLSresult = LoadFTmat...
%         ([handles.paths.PLSresultsDir,'/',PLSresultFiles(1).name], 'ViewPLSresults');
% else
%     ErrMsg = {'Error:'; '';
%         'Multiple PLS result .mat files found in the target directory.';
%         'Each PLS run should be kept in its own respective folder.'; ''};
%     msgbox(ErrMsg, 'Error:');
%     return;
% end

% Plot singular values for each LV in PLS run:
figure;
bar(PLSresult.s);
hold on;

% Get labels:
%** title([handles.paths.PLSresultsDir,'/',PLSresultFiles(1).name], 'Interpreter', 'none');
title([handles.paths.PLSresultsDir,'/',CurrentPLSres], 'Interpreter', 'none');

for LV = 1:size(PLSresult.v, 2)
    LVstring{LV} = ['LV',num2str(LV)];
end

set(gca, 'XTickLabel', LVstring);
ylabel('Singular Values');
xlabel('Latent Variables:');

hold off;

% Remove ErrorLog if no problems:
if exist([pwd,'/ErrorLog_ViewPLSresults.txt'], 'file');
    LogCheck = dir('ErrorLog_ViewPLSresults.txt');
    if LogCheck.bytes == 0
        delete('ErrorLog_ViewPLSresults.txt');
    end
end 




%============================%
% GUIDE CREATEFCN FUNCTIONS: %
%============================%

% --- Executes during object creation, after setting all properties.
function TextboxAnatFile_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function TextboxPLSresultsDir_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function ListboxPLSresultsFiles_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function ListboxGrpAvgCorrFiles_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

