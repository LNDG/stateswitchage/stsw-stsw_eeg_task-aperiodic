function varargout = VisualizeSourceData(varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Visualization HUB for source analysis data.  %
% Last modified: Feb.26, 2014                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Usage: 
%  VisualizeSourceData(BuilderMatFile)
%
% Inputs:
%  BuilderMatFile = Can be empty or specify target Builder .mat file.


% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


% Last Modified by GUIDE v2.5 16-Sep-2014 11:25:43

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @VisualizeSourceData_OpeningFcn, ...
                   'gui_OutputFcn',  @VisualizeSourceData_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


%--- Executes just before VisualizeSourceData is made visible. ---%
%-------------------------------------------------------%
function VisualizeSourceData_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to VisualizeSourceData (see VARARGIN)

% Choose default command line output for VisualizeSourceData
handles.output = hObject;


% Make sure toolbox paths are added:
[PipelineDir, ~, ~] = fileparts(which('VisualizeSourceData.m'));

addpath(genpath(PipelineDir));
rmpath([PipelineDir,'/DEFAULT_SETTINGS']);  % Make sure its calling from AnalysisID
rmpath([PipelineDir,'/TEMPORARY_FIXES']);   % Make sure its calling from FT toolbox

CheckToolboxPaths(PipelineDir);


% Initialize variables and default settings:
handles.name.GroupID = [];
handles.name.SubjID  = [];
handles.name.CondID  = [];
handles.paths        = [];

handles.gui.SubjIDAvgList     = [];  % SubjID list with GrpAvg at end of list.
handles.gui.FuncOverlayFile   = [];
handles.gui.FuncDisplayPath   = [];
handles.gui.FuncOverlayLegend = [];
handles.gui.AnatUnderlayFile  = [];
handles.gui.TempViewFolder    = [];

handles.gui.GrpAvgUnderlay = ...  % Default AnatUnderlay file for GrpAvg func images.
    [PipelineDir,'/MEGPLS_TOOLBOX/Masks_Templates/template_ch2+tlrc.BRIK'];

handles.gui.PlotOverlap = 'no';  % For legend construction
handles.gui.PlotGrpAvg  = 'no';

% Check for BuilderMat input:
if isempty(varargin)
    handles.gui.BuilderMat = [];
    
elseif numel(varargin) > 1
    disp('Error: Input should be empty or a Builder .mat file.')
    error('Incorrect number of inputs.')
    
elseif exist(varargin{1}, 'file')
    handles.gui.BuilderMat = varargin{1};
    handles = LoadBuilderMat(handles, varargin{1});
end

% Update handles structure
guidata(hObject, handles);


% UIWAIT makes VisualizeSourceData wait for user response (see UIRESUME)
% uiwait(handles.figure1);


%--- Outputs from this function are returned to the command line. ---%
%--------------------------------------------------------------------%
function varargout = VisualizeSourceData_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;




%=====================================%
% FUNCTIONS FOR LOADING BUILDER .MAT: %
%=====================================%

%--- Textbox to display selected path of Builder: ---%
%----------------------------------------------------%
function TextboxBuilderMat_Callback(hObject, eventdata, handles)
EnteredText = get(handles.TextboxBuilderMat, 'String');

if ~isequal(EnteredText, handles.gui.BuilderMat)
    set(handles.TextboxBuilderMat, 'String', handles.gui.BuilderMat);
    msgbox('Note: Use the button to change Builder .mat file.')
end


%--- Executes on button press in ButtonLoadBuilderMat. ---%
%---------------------------------------------------------%
function ButtonLoadBuilderMat_Callback(hObject, eventdata, handles)
[BuilderFile, BuilderPath] = uigetfile('Builder_*.mat',...
	'Select Builder .mat file to load:', 'MultiSelect', 'off');

if BuilderFile == 0
	return;  % If user cancels
else
    handles.gui.BuilderMat = [BuilderPath,BuilderFile];
	set(handles.TextboxBuilderMat, 'String', handles.gui.BuilderMat);
    
    handles = LoadBuilderMat(handles, handles.gui.BuilderMat);
    guidata(hObject, handles);
end


%--- Loads variables from Builder .mat file. ---%
%-----------------------------------------------%
function OutputHandles = LoadBuilderMat(InputHandles, BuilderMat)
handles = InputHandles;

% Reset variables to default:
handles.name.GroupID = [];
handles.name.SubjID  = [];
handles.name.CondID  = [];
handles.paths        = [];

handles.gui.SubjIDAvgList     = [];  % SubjID list with GrpAvg at end of list.
handles.gui.TempViewFolder    = [];
handles.gui.AnatUnderlayFile  = [];
handles.gui.FuncOverlayFile   = [];
handles.gui.FuncOverlayLegend = [];

handles.gui.FuncDisplayPath   = [];
set(handles.TextboxFuncImageFile, 'String', []);

[PipelineDir, ~, ~]        = fileparts(which('VisualizeSourceData.m'));
handles.gui.GrpAvgUnderlay = ...  % Default AnatUnderlay file for GrpAvg func images.
    [PipelineDir,'/MEGPLS_TOOLBOX/Masks_Templates/template_ch2+tlrc.BRIK'];

handles.gui.PlotOverlap = 'no';  % For legend construction
handles.gui.PlotGrpAvg  = 'no';

% Load Builder .mat:
LoadBuilder   = load(BuilderMat);
handles.name  = LoadBuilder.name;
handles.paths = LoadBuilder.paths;

% Update GUI:
GroupIndex = get(handles.ListboxGroupID, 'Value');
set(handles.ListboxSubjID, 'Value', length(handles.name.SubjID{GroupIndex})+1);

handles = UpdateNameIDs(handles);
set(handles.TextboxBuilderMat, 'String', handles.gui.BuilderMat);

% Set output handles:
OutputHandles = handles;




%============================%
% FUNCTIONS FOR ID DISPLAYS: %
%============================%

%--- Executes on selection change in ListboxGroupID. ---%
%-------------------------------------------------------%
function ListboxGroupID_Callback(hObject, eventdata, handles)
handles = UpdateNameIDs(handles);
guidata(hObject, handles);


%--- Executes on selection change in ListboxSubjID. ---%
%------------------------------------------------------%
function ListboxSubjID_Callback(hObject, eventdata, handles)
handles = UpdateNameIDs(handles);
guidata(hObject, handles);


%--- Executes on selection change in ListboxCondID. ---%
%------------------------------------------------------%
function ListboxCondID_Callback(hObject, eventdata, handles)
handles = UpdateNameIDs(handles);
guidata(hObject, handles);


%--- Update GroupID's, SubjID's, and CondID's: ---%
%-------------------------------------------------%
function OutputHandles = UpdateNameIDs(InputHandles)
handles = InputHandles;

% Update GroupID listbox:
set(handles.ListboxGroupID, 'String', handles.name.GroupID);

GroupIndex    = get(handles.ListboxGroupID, 'Value');
MaxGroupIndex = length(handles.name.GroupID);

if isempty(GroupIndex) || GroupIndex == 0 || GroupIndex > MaxGroupIndex
    set(handles.ListboxGroupID, 'Value', MaxGroupIndex);
end

% Update SubjID listbox:
% Note: Group-averages do not exist for raw sources.
if isempty(handles.name.SubjID)
    set(handles.ListboxSubjID, 'String', []);
    MaxSubjIndex = 0;
    
elseif get(handles.ButtonViewRawSources, 'Value') == 1  % Raw sources do not have GPAVG.
    set(handles.ListboxSubjID, 'String', handles.name.SubjID{GroupIndex});
    MaxSubjIndex = length(handles.name.SubjID{GroupIndex});
    
elseif get(handles.ButtonViewNormSources, 'Value') == 1  % Add NormSource GPAVG to list.
    handles.gui.SubjIDAvgList{GroupIndex} = [handles.name.SubjID{GroupIndex}; 'GroupAvg'];
    set(handles.ListboxSubjID, 'String', handles.gui.SubjIDAvgList{GroupIndex});
    MaxSubjIndex = length(handles.gui.SubjIDAvgList{GroupIndex});
end

SubjIndices = get(handles.ListboxSubjID, 'Value');
if isempty(SubjIndices) || max(SubjIndices) == 0 || max(SubjIndices) > MaxSubjIndex
    set(handles.ListboxSubjID, 'Value', MaxSubjIndex);
end

% Update CondID listbox:
set(handles.ListboxCondID, 'String', handles.name.CondID);

CondIndices  = get(handles.ListboxCondID, 'Value');
MaxCondIndex = length(handles.name.CondID);

if isempty(CondIndices) || max(CondIndices) == 0 || max(CondIndices) > MaxCondIndex
    set(handles.ListboxCondID, 'Value', MaxCondIndex);
end


% Check SubjID & CondID multi-selection:
% For raw sources, allow only multiple CondID selection for voxel timeseries plots.
% Cannot overlay SubjIDs for raw sources since each subject is in its own space.
if get(handles.ButtonViewRawSources, 'Value') == 1
    if length(SubjIndices) > 1
        SubjIndices = 1;
        set(handles.ListboxSubjID, 'Value', 1);
    end
end

if length(SubjIndices) == 1 && length(CondIndices) == 1
    handles.gui.PlotOverlap = 'no';
    
elseif length(SubjIndices) > 1 && length(CondIndices) == 1
    handles.gui.PlotOverlap = 'SubjIDs';
    
elseif length(SubjIndices) == 1 && length(CondIndices) > 1
    handles.gui.PlotOverlap = 'CondIDs';
    
elseif length(SubjIndices) > 1 && length(CondIndices) > 1
    handles.gui.PlotOverlap = 'Both';
end
    

% Check if GroupAvg is selected:
if get(handles.ButtonViewRawSources, 'Value') == 1 || isempty(handles.gui.SubjIDAvgList)
    handles.gui.PlotGrpAvg = 'no';
else
    GroupIndex  = get(handles.ListboxGroupID, 'Value');
    GrpAvgIndex = length(handles.gui.SubjIDAvgList{GroupIndex});
    
    if ismember(GrpAvgIndex, SubjIndices)
        handles.gui.PlotGrpAvg = 'yes';
    else
        handles.gui.PlotGrpAvg = 'no';
    end
end


% Set output handles:
OutputHandles = handles;




%=========================================================%
% FUNCTIONS FOR FUNCTIONAL OVERLAY & ANATOMICAL UNDERLAY: %
%=========================================================%

%--- Executes on button press in ButtonSetFuncOverlay. ---%
%---------------------------------------------------------%
function ButtonSetFuncOverlay_Callback(hObject, eventdata, handles)
if isempty(handles.gui.BuilderMat)
    msgbox('Warning: Select Builder .mat file first.', 'Warning:');
    return;
end

% Make sure selection parameters updated:
handles = UpdateNameIDs(handles);

% Get selected indices:
g = get(handles.ListboxGroupID, 'Value');
s = get(handles.ListboxSubjID,  'Value');
c = get(handles.ListboxCondID,  'Value');

if length(s) > 1 || length(c) > 1
    msgbox('Warning: Only one image file can be set as the functional overlay.');
    return;
end

% Acquire dataset paths for functional & anatomical image:
if get(handles.ButtonViewRawSources, 'Value') == 1
    FuncOverlay  = handles.paths.Afni4DSource{g}{s,c};
    AnatUnderlay = handles.paths.MRIdataAfni{g}{s};
    
elseif get(handles.ButtonViewNormSources, 'Value') == 1
    if strcmp(handles.gui.PlotGrpAvg, 'no')
        FuncOverlay  = handles.paths.Afni4DNormSource{g}{s,c};
        AnatUnderlay = handles.paths.NormMRIAfni{g}{s};
    else
        FuncOverlay  = handles.paths.Afni4DGrpAvg{g}{c};
        AnatUnderlay = handles.gui.GrpAvgUnderlay;
    end
end

% Check if files exist:
if ~exist(FuncOverlay, 'file')
    ErrMsg = {'Error: AFNI functional image missing for selected dataset.';
        ['- File: ',FuncOverlay]};
    msgbox(ErrMsg, 'Error:');
    return;
end
if ~exist(AnatUnderlay, 'file')
    ErrMsg = {'Error: AFNI anatomical image missing for selected dataset.';
        ['- File: ',AnatUnderlay]};
    msgbox(ErrMsg, 'Error:');
    return;
end

% Set overlay and underlay files:
[~, Name, Ext] = fileparts(FuncOverlay);

handles.gui.FuncDisplayPath = ...
    ['../',handles.name.GroupID{g},'/',handles.name.CondID{c},'/',Name,Ext];

handles.gui.FuncOverlayFile  = FuncOverlay;
handles.gui.AnatUnderlayFile = AnatUnderlay;
set(handles.TextboxFuncImageFile, 'String', handles.gui.FuncDisplayPath);

% Set temp viewing folder:
if get(handles.ButtonViewRawSources, 'Value') == 1
    handles.gui.TempViewFolder = [handles.paths.AnalysisID,'/TEMP_VIEWER_FILES/'...
        'RAW_SOURCES/',handles.name.GroupID{g},'/',handles.name.CondID{c}];
    
elseif get(handles.ButtonViewNormSources, 'Value') == 1
    handles.gui.TempViewFolder = [handles.paths.AnalysisID,'/TEMP_VIEWER_FILES/'...
        'NORM_SOURCES/',handles.name.GroupID{g},'/',handles.name.CondID{c}];
end

% Set legend name for FuncOverlay file in TSplot:
if strcmp(handles.gui.PlotGrpAvg, 'yes') && s == length(handles.gui.SubjIDAvgList{g})
    SubjNameLeg = 'GrpAvg';
else
    SubjNameLeg = handles.name.SubjID{g}{s};
end

switch handles.gui.PlotOverlap
    case {'no', 'Both'}
        handles.gui.FuncOverlayLegend = ...
            [SubjNameLeg,'_',handles.name.CondID{c}];
    case 'SubjIDs'
        handles.gui.FuncOverlayLegend = SubjNameLeg;
    case 'CondIDs'
        handles.gui.FuncOverlayLegend = handles.name.CondID{c};
end

% Save handles:
guidata(hObject, handles);


%--- Textbox to display dataset selected as functional overlay. ---%
%------------------------------------------------------------------%
function TextboxFuncImageFile_Callback(hObject, eventdata, handles)
EnteredText = get(handles.TextboxFuncImageFile, 'String');

if ~isequal(EnteredText, handles.gui.FuncDisplayPath)
    set(handles.TextboxFuncImageFile, 'String', handles.gui.FuncDisplayPath);
    msgbox('Note: Use button to change functional image file.');
end


%--- Compile other timeseries datasets for View4D TSplot: ---%
%------------------------------------------------------------%
function [OtherTSData, OtherLegendData, ErrMsg] = GetOtherTimeseriesData(InputHandles)
handles = InputHandles;
paths   = handles.paths;

GroupIndex  = get(handles.ListboxGroupID, 'Value');
SubjIndices = get(handles.ListboxSubjID,  'Value');
CondIndices = get(handles.ListboxCondID,  'Value');

DataIndex    = 1;
MissingFiles = {};

% Compile input data:
for s = SubjIndices
    for c = CondIndices
        
        % Compile other input data for TSplots:
        if get(handles.ButtonViewRawSources, 'Value') == 1
            OtherTSData{DataIndex} = paths.Afni4DSource{GroupIndex}{s,c};
            
        elseif get(handles.ButtonViewNormSources, 'Value') == 1
            if strcmp(handles.gui.PlotGrpAvg, 'yes') && ...
                    s == length(handles.gui.SubjIDAvgList{GroupIndex})
                OtherTSData{DataIndex} = paths.Afni4DGrpAvg{GroupIndex}{c};
            else
                OtherTSData{DataIndex} = paths.Afni4DNormSource{GroupIndex}{s,c};
            end  
        end
        
        % Compile legend for other input data:
        if strcmp(handles.gui.PlotGrpAvg, 'yes') && ...
                s == length(handles.gui.SubjIDAvgList{GroupIndex})
            SubjNameLeg = 'GrpAvg';
        else
            SubjNameLeg = handles.name.SubjID{GroupIndex}{s};
        end

        switch handles.gui.PlotOverlap
            case {'no', 'Both'}
                OtherLegendData{DataIndex} = ...
                    [SubjNameLeg,'_',handles.name.CondID{c}];
            case 'SubjIDs'
                OtherLegendData{DataIndex} = SubjNameLeg;
            case 'CondIDs'
                OtherLegendData{DataIndex} = handles.name.CondID{c};
        end
        
        DataIndex = DataIndex + 1;
        
    end
end

% If functional overlay included, remove from "OtherTSData" list.
% Recall: functional overlay is specified separately in View4D_BRIK function.
if ismember(handles.gui.FuncOverlayFile, OtherTSData)
    CheckIndex = find(ismember(OtherTSData, handles.gui.FuncOverlayFile));
    
    if ~isempty(CheckIndex)
        OtherTSData(CheckIndex)      = [];
        OtherLegendData(CheckIndex)  = [];
    end
end

% Make sure data exists:
for d = 1:length(OtherTSData)
    if ~exist(OtherTSData{d}, 'file')
        MissingFiles = [MissingFiles; OtherTSData{d}];
    end
end

if ~isempty(MissingFiles)
    ErrMsg = {'ERROR: Some of the selected dataset(s) could not be found.'};
    ErrMsg = [ErrMsg; MissingFiles];
else
    ErrMsg = [];
end




%=====================================%
% FUNCTIONS FOR PLOTTING SOURCE DATA: %
%=====================================%

%--- Executes on button press in ButtonTimeLegend. ---%
%-----------------------------------------------------%
function ButtonTimeLegend_Callback(hObject, eventdata, handles)
if isempty(handles.gui.BuilderMat)
    msgbox('Warning: Select Builder .mat file first.', 'Warning:');
    return;
end

if exist([handles.paths.AnalysisID,'/NiftiAfni4D_TimeLegend.txt'], 'file')
    open([handles.paths.AnalysisID,'/NiftiAfni4D_TimeLegend.txt']);
else
    msgbox('Error: TimeLegend file could not be found.', 'Error:');
    return;
end


%--- Executes when selected object is changed in PanelPlotSources. ---%
%---------------------------------------------------------------------%
function PanelPlotSources_SelectionChangeFcn(hObject, eventdata, handles)

% For raw sources, can only overlay condition voxel timeseries (since non-normalised):
if get(handles.ButtonViewRawSources, 'Value') == 1
    set(handles.ListboxSubjID, 'Max', 1);  % Disable SubjID multi-selection
    set(handles.ListboxCondID, 'Max', 2);  % Enable CondID multi-selection
    
    GroupIndex  = get(handles.ListboxGroupID, 'Value');
    SubjIndices = get(handles.ListboxSubjID,  'Value');
    
    if length(SubjIndices) > 1
        set(handles.ListboxSubjID, 'Value', 1);
    end
end

% For normalised sources, can overlay both subject & condition voxel timeseries:
if get(handles.ButtonViewNormSources, 'Value') == 1
    set(handles.ListboxSubjID, 'Max', 2);
    set(handles.ListboxCondID, 'Max', 2);
end

% Reset functional overlay selection:
handles.gui.FuncOverlayFile   = [];
handles.gui.FuncOverlayLegend = [];
handles.gui.AnatUnderlayFile  = [];
handles.gui.TempViewFolder    = [];

handles.gui.FuncDisplayPath   = [];
set(handles.TextboxFuncImageFile, 'String', []);

% Update handles:
handles = UpdateNameIDs(handles);
guidata(hObject, handles);


%--- Executes on button press in ButtonViewRawSources.
function ButtonViewRawSources_Callback(hObject, eventdata, handles)

%--- Executes on button press in ButtonViewNormSources.
function ButtonViewNormSources_Callback(hObject, eventdata, handles)


%--- Executes on button press in ButtonView4DBrik. ---%
%-----------------------------------------------------%
function ButtonView4DBrik_Callback(hObject, eventdata, handles)
if isempty(handles.gui.BuilderMat)
    msgbox('Warning: Select Builder .mat file first.', 'Warning:');
    return;
end
if isempty(handles.gui.FuncOverlayFile)
    msgbox('Warning: Select dataset to act as functional overlay.', 'Warning:');
    return;
end
if isempty(handles.gui.AnatUnderlayFile)
    msgbox('Warning: Anatomical underlay file not selected.', 'Warning:');
    return;
end

% Check if paths have spaces in them (AFNI paths cannot support spaces):
CheckSpaces1 = strfind(handles.gui.AnatUnderlayFile, ' ');
CheckSpaces2 = strfind(handles.gui.FuncOverlayFile, ' ');
CheckSpaces3 = strfind(handles.gui.TempViewFolder, ' ');
if ~isempty(CheckSpaces1) || ~isempty(CheckSpaces2) || ~isempty(CheckSpaces3)
    msgbox('Error: AFNI fcns cannot read folder & file paths containing spaces.', 'Error:')
    return;
end

% Check if file is 4D:
CurrentDir = pwd;
Opt.format = 'vector';

[~, FuncImg, ~, ~] = BrikLoad(handles.gui.FuncOverlayFile, Opt);

if numel(size(FuncImg)) ~= 4
    msgbox('Error: Selected file is not 4D file. Use "View in AFNI" instead.');
    return;
end

% Create temp viewing directory:
if ~exist(handles.gui.TempViewFolder, 'dir')
    status = mkdir(handles.gui.TempViewFolder);
    
    if status == 0
        msgbox('Error: Failed to create viewing folder in selected directory.');
        return;
    end
end

% Get temp file paths:
[~, AnatFile, AnatExt] = fileparts(handles.gui.AnatUnderlayFile);
TempAnatFile           = [handles.gui.TempViewFolder,'/',AnatFile,AnatExt];

if exist(TempAnatFile, 'file')
    delete(TempAnatFile);
    delete([handles.gui.TempViewFolder,'/',AnatFile,'.HEAD']);
end

% Copy anatomy file to temp view folder & resample if needed:
system(['3dcopy ',handles.gui.AnatUnderlayFile,' ',TempAnatFile]);
TempAnatFile = MEGpipeline_AfniResampleAnat(TempAnatFile, handles.gui.FuncOverlayFile);

if isempty(TempAnatFile)
    msgbox('Warning: Failed to resample anatomy file.', 'Error:');
end

% Compile other datasets for timeseries plot:
[OtherTSData, OtherLegendData, ErrMsg] = GetOtherTimeseriesData(handles)
if ~isempty(ErrMsg)
    msgbox(ErrMsg, 'Error:');
    return;
end

% Get full legend data:
FullLegendData = [handles.gui.FuncOverlayLegend; OtherLegendData'];

% Plot data:
View4D_BRIK(TempAnatFile, handles.gui.FuncOverlayFile, OtherTSData, FullLegendData);


%--- Executes on button press in ButtonAFNIViewer. ---%
%-----------------------------------------------------%
function ButtonAFNIViewer_Callback(hObject, eventdata, handles)
if isempty(handles.gui.BuilderMat)
    msgbox('Warning: Select Builder .mat file first.', 'Warning:');
    return;
end
if isempty(handles.gui.FuncOverlayFile)
    msgbox('Warning: Select dataset to act as functional overlay.', 'Warning:');
    return;
end
if isempty(handles.gui.AnatUnderlayFile)
    msgbox('Warning: Anatomical underlay file not selected.', 'Warning:');
    return;
end

% Check if paths have spaces in them (AFNI paths cannot support spaces):
CheckSpaces1 = strfind(handles.gui.AnatUnderlayFile, ' ');
CheckSpaces2 = strfind(handles.gui.FuncOverlayFile, ' ');
CheckSpaces3 = strfind(handles.gui.TempViewFolder, ' ');
if ~isempty(CheckSpaces1) || ~isempty(CheckSpaces2) || ~isempty(CheckSpaces3)
    msgbox('Error: AFNI fcns cannot read folder & file paths containing spaces.', 'Error:')
    return;
end

% Create temp viewing directory:
if ~exist(handles.gui.TempViewFolder, 'dir')
    status = mkdir(handles.gui.TempViewFolder);
    
    if status == 0
        msgbox('Error: Failed to create viewing folder in selected directory.');
        return;
    end
end

% Get temp file paths:
[~, AnatFile, AnatExt] = fileparts(handles.gui.AnatUnderlayFile);
[~, FuncFile, FuncExt] = fileparts(handles.gui.FuncOverlayFile);

TempAnatFile = [handles.gui.TempViewFolder,'/',AnatFile,AnatExt];
TempFuncFile = [handles.gui.TempViewFolder,'/',FuncFile,FuncExt];

if exist(TempAnatFile, 'file')
    delete(TempAnatFile);
    delete([handles.gui.TempViewFolder,'/',AnatFile,'.HEAD']);
end
if exist(TempFuncFile, 'file')
    delete(TempFuncFile);
    delete([handles.gui.TempViewFolder,'/',FuncFile,'.HEAD']);
end

% Copy func file to temp view folder & convert to AFNI if needed:
system(['3dcopy ',handles.gui.FuncOverlayFile,' ',TempFuncFile]);

% Copy anatomy file to temp view folder & resample if needed:
% Note: Do not change TempAnatFile to resampled version (want high-res as default input).
system(['3dcopy ',handles.gui.AnatUnderlayFile,' ',TempAnatFile]);
ResampledAnat = MEGpipeline_AfniResampleAnat(TempAnatFile, TempFuncFile);

if isempty(ResampledAnat)
    msgbox('Warning: Failed to resample anatomy file.', 'Error:');
end

% Prepare file calls to AFNI:
[~, AnatName, ~]    = fileparts(TempAnatFile);
AnatName(end-4:end) = [];  % Remove +view label

[~, FuncName, ~]    = fileparts(TempFuncFile);
FuncName(end-4:end) = [];

% Set environmental variables:
setenv('AFNI_SLAVE_FUNCTIME',    'YES');    % Locks overlay with time-index (Newer versions).
setenv('AFNI_SLAVE_BUCKETS_TOO', 'YES');    % Locks overlay with time-index (Older versions).
setenv('AFNI_SLAVE_THRTIME',     'YES');    % Locks threshold with time-index (Older versions).
setenv('AFNI_SLAVE_THROLAY',     'OLay');   % Locks threshold with time-index (Newer versions).
setenv('AFNI_LEFT_IS_LEFT',      'YES');    % Sets images to neurological view.
setenv('AFNI_THRESH_AUTO',       'NO');     % Threshold slider does not change automatically.
setenv('AFNI_THRESH_LOCK',       'VALUE');
setenv('AFNI_FLOATSCAN',         'YES');

if strcmp(getenv('AFNI_LEFT_IS_LEFT'), 'NO')
    msgbox('NOTE: "AFNI_LEFT_IS_LEFT" env. variable is now set to ''YES''.')
end

% Open AFNI command:
CmdLine = ['afni -com '''               ...
    'OPEN_WINDOW A.axialimage;'        ...
    ' OPEN_WINDOW A.sagittalimage;'     ...
    ' OPEN_WINDOW A.coronalimage;'      ...
    ' SWITCH_UNDERLAY A.',AnatName,';'  ...  % Underlay .BRIK
    ' SWITCH_OVERLAY A.',FuncName,';'   ...  % Overlay .BRIK
    ' SET_SUBBRICKS A 1 1 1;'           ...  % Sets .BRIK index to 1
    ' SET_FUNC_AUTORANGE A.+;'          ...  % Turns ON autorange
    ' REDISPLAY;'' &'];
    
CurrentDir = pwd;
cd(handles.gui.TempViewFolder);

system(CmdLine);
cd(CurrentDir);




%==============================%
% GUIDE "CREATEFCN" FUNCTIONS: %
%==============================%

% --- Executes during object creation, after setting all properties.
function TextboxBuilderMat_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function ListboxGroupID_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function ListboxSubjID_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes during object creation, after setting all properties.
function ListboxCondID_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function TextboxFuncImageFile_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
