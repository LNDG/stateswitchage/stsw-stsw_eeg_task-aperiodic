


=====================================
COMPATIBILITY FIX FOR "WARNING_ONCE":
=====================================

Currently, there is a bug with Fieldtrip functions being called through MATLAB GUIDE interfaces.
 - Occurs in the "warning_once" called by "ft_postamble" after main Fieldtrip functions.
 - Tries to set the GUIDE callback (contains brackets & symbols) as a fieldname to "ft_previous_warnings".
 - Link, but not answered yet: http://mailman.science.ru.nl/pipermail/fieldtrip/2013-August/006889.html

Temporary fix is currently applied by "SetPathsGUI.m":
 1) Backup of current "warning_once.m" files are made (Renamed to "warning_once_original.m").
    Files are found in:  "../Fieldtrip_Path/private" and "../Fieldtrip_Path/utilities/private"

 2) Replaced by older working version of "warning_once.m" from "TEMPORARY_FIXES" folder.
    Older version of "warning_once.m" is from Fieldtrip-20130211.

 3) The above is repeated for "ft_postamble_history.m" as well in "../Fieldtrip_Path"
    Older version of "ft_postamble_history.m" is also from Fieldtrip-20130211.


