%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADVANCED SETTINGS FOR ICA DECOMPOSITION: %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This settings file will be read by the ICAcleanerGUI.
% - Settings here will be applied to ICA decompositions.
%
% Usage:
%  a)  Settings under "DO NOT EDIT" are handled by [MEG]PLS. Do not alter these.
%  b)  Change other values as needed. See "help" command for FieldTrip section.
%  c)  Optional settings can be uncommented and specified.
%
function [cfgResampleICA, cfgICA] = Settings_ICA



%--- "FT_RESAMPLEDATA" SETTINGS: ---%
%-----------------------------------%

% GENERAL SETTINGS:
cfgResampleICA.resamplefs = 300;	 % Sampling rate of data for ICA decomposition.
cfgResampleICA.trials     = 'all';
cfgResampleICA.detrend    = 'no';    % Disable if you demeaned / detrended during preprocessing.
cfgResampleICA.demean     = 'no';	 % Enable only if you didn't demean during preprocessing.
cfgResampleICA.feedback   = 'text';


% OPTIONAL IF SPECIFYING TIME-AXIS INSTEAD:
%  cfgResampleICA.time
%  cfgResampleICA.method



%--- "FT_COMPONENTANALYSIS" SETTINGS: ---%
%----------------------------------------%

% DO NOT EDIT:
cfgICA.channel		= 'MEG';
%cfgICA.unmixing
%cfgICA.topolabel


% GENERAL SETTINGS:
cfgICA.method       = 'runica';  % See "help ft_componentanalysis" for list of other options.
cfgICA.numcomponent = 'all';     % Warning: binica only works on linux (use runica for windows).
cfgICA.trials       = 'all';
cfgICA.demean       = 'no';      % Set to 'yes' if you didn't demean during preprocessing.


% NOTE: Below are optional FieldTrip & EEGLAB settings for each method.
% - For more information, see EEGLAB help functions for respective methods.


% OPTIONAL SETTINGS FOR BINICA:
%  cfgICA.binica.extended
%  cfgICA.binica.pca
%  cfgICA.binica.sphering
%  cfgICA.binica.lrate
%  cfgICA.binica.blocksize
%  cfgICA.binica.maxsteps
%  cfgICA.binica.stop
%  cfgICA.binica.weightsin
%  cfgICA.binica.verbose
%  cfgICA.binica.filenum
%  cfgICA.binica.posact
%  cfgICA.binica.annealstep
%  cfgICA.binica.annealdeg
%  cfgICA.binica.bias
%  cfgICA.binica.momentum
	

% OPTIONAL SETTINGS FOR RUNICA:
%  cfgICA.runica.extended
%  cfgICA.runica.pca
%  cfgICA.runica.sphering
%  cfgICA.runica.weights
%  cfgICA.runica.lrate
%  cfgICA.runica.block
%  cfgICA.runica.anneal
%  cfgICA.runica.annealdeg
%  cfgICA.runica.stop
%  cfgICA.runica.maxsteps
%  cfgICA.runica.bias
%  cfgICA.runica.momentum
%  cfgICA.runica.specgram
%  cfgICA.runica.posact
%  cfgICA.runica.verbose
%  cfgICA.runica.logfile
%  cfgICA.runica.interput


% OPTIONAL SETTINGS FOR FASTICA:
%  cfgICA.fastica.approach
%  cfgICA.fastica.numOfIC
%  cfgICA.fastica.g
%  cfgICA.fastica.finetune
%  cfgICA.fastica.a1
%  cfgICA.fastica.a2
%  cfgICA.fastica.mu
%  cfgICA.fastica.stabilization
%  cfgICA.fastica.epsilon
%  cfgICA.fastica.maxNumIterations
%  cfgICA.fastica.maxFinetune
%  cfgICA.fastica.sampleSize
%  cfgICA.fastica.initGuess
%  cfgICA.fastica.verbose
%  cfgICA.fastica.displayMode
%  cfgICA.fastica.displayInterval
%  cfgICA.fastica.firstEig
%  cfgICA.fastica.lastEig
%  cfgICA.fastica.interactivePCA
%  cfgICA.fastica.pcaE
%  cfgICA.fastica.pcaD
%  cfgICA.fastica.whiteSig
%  cfgICA.fastica.whiteMat
%  cfgICA.fastica.dewhiteMat
%  cfgICA.fastica.only


% OPTIONAL SETTINGS FOR ICASSO (See "help ICASSOEST"):
% Runs FastICA a specified number of times and provides info about component stability.
%  cfgICA.icasso.mode
%  cfgICA.icasso.Niter


% OPTIONAL SETTINGS FOR DDS (See "help dds_create_state"):
%  cfgICA.dss.denf.function
%  cfgICA.dss.denf.params


% OPTIONAL SETTINGS FOR SOBI:
%  cfgICA.sobi.n_sources
%  cfgICA.sobi.p_correlations


% OPTIONAL SETTINGS FOR CSP (COMMON-SPATIAL PATTERNS):
%  cfgICA.csp.classlabels = vector that assigns a trial to class 1 or 2.
%  cfgICA.csp.numfilters  = the number of spatial filters to use (default: 6).


% SEED FOR RANDOM NUMBERS:
%  cfgICA.randomseed   = integer seed value of user's choice
%  cfgICA.randomseed   = comp.cfg.callinfo.randomseed (from previous call)
