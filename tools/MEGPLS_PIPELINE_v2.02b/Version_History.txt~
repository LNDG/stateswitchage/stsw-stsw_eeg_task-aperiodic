
=============
VERSION 2.02b
=============

- Brain scores and singular values can now be viewed even if multiple PLSres .mat files are in the same folder.
  Previously, this would be an issue for viewing results from seed or behavioural PLS runs, where multiple PLSres .mat
  files are output into the same folder.

- Updated PLS behaviour .xls template instructions.



============
VERSION 2.02
============

- HOTFIX: Fixed an error that would occur if the Parallel Processing Toolbox was not available.
  Should now work regardless of whether the Matlab Parallel toolbox is available or not.

- Modified the ErrorLog check in each [MEG]PLS component:
  Fixes an error with ErrorLogs that would occur if multiple versions of [MEG]PLS are on the MATLAB path.



============
VERSION 2.01
============

- HOTFIX: Fixed an error that would occur during installation.



============
VERSION 2.00
============

--- MAJOR ADDITIONS: ---
------------------------

- NEW CLEANING MODULE:  ICA artifact cleaning module:  
  GUI module for accessible & streamlined ICA decomposition, component visualization & removal.

- NEW CLEANING MODULE:  Bad Channel & Trial Removal module:
  GUI module for easy removal of bad channels & trials.


- PLS toolbox called by [MEG]PLS is now synced to use the Baycrest PLScmd toolbox.
  Prior to this, modified versions of the primary PLS script were being used.

- Mask generated for PLS is now "fitted" directly to your input data:
  Only voxels that % threshold of datasets share (adjustable by user) are included in the PLS analysis.

- New PLS visualization features:
  Alongside bootstrap-ratio images, PLS will now also output salience images for each LV.
  Added buttons to plot brain scores and singular values for each LV.

- Users can now specify a time-range or multiple time-indices for Seed PLS coordinates.
  For a given seed, data will be extracted from time-indices of interest and averaged for PLS seedmat.
  This allows users to correlate brain activity with activity from specific regions across a time-range.
  Note: Instructions for seed & behaviour .xls template files have been updated for above changes.


- Greatly improved epoching functionality of MEG preprocessing module:
  Users should now be able to define complex trials within the [MEG]PLS GUI (see README).
  Added functionality for multi-stage epoching (see README).
  Much more feedback (Header & event info, continuous or epoched, # trials, etc).

- Performance optimizations for each [MEG]PLS module & analysis component:
  Interactions with various GUI modules should now be much faster.
  Optimized memory usage for each component.

- Various improvements to parallel processing performance:
  Improved memory usage for multiple workers.
  Incorporated more sections of [MEG]PLS to utilize PCT.

- Integration of parallel processing toolbox with [MEG]PLS GUI:
  If available, users will now be asked if they with to open a pool of workers.


- Global mean field power is now computed during timelock analyses.
  Added viewing GFMP viewing functionality to ViewTimeFreq viewer as well.

- Non-normalised sources & MRI files are now automatically deobliqued for viewing.
  Corrects previous issue where raw source & MRI files were misaligned during visualization.

- Added coordinate system dropdown box for MRI coregistration.
  User now selected desired coordinate system along with the interactive viewer.

- Added TimeLegend button to ViewSourceData viewer.


- Changed labels in GUI modules to be more intuitive.

- More feedback & error-check for various [MEG]PLS modules.

- Progress bars have been added for each analysis section.
  Users will now see % completion for each stage of the pipeline.

- Updated default settings on various GUI modules to reflect settings more commonly-used.

- Auto-detection of various settings:
  Ex:  Filter-padding length, trial overlap threshold, etc.



--- BUG & ERROR FIXES: ---
--------------------------

- Fixed issue where GUI modules were not rendering elements in their correct positions.
- Fixed various sync issues between listboxes in GUI modules.

- Fixed error in PreprocMEG where overlap threshold and inc./exc. event times were not updating properly.
- Fixed error in Builder when attempting to load Groups & Subjects from another Builder .mat file.
- Fixed error in Builder that would sometimes occur when saving settings with more than 1 group.
- Fixed error in Builder when folder containing no DataID's is selected as the Rootpath.
- Fixed error in Builder that would occur when using a whole trial mean baseline, but had no specified time.

- Fixed error in PLS that would occur for MATLAB versions 2013 and above (RandStream error).
- Fixed PLS result issue where significant LV's would not be written out into AFNI format.
- Fixed error that would sometimes occur when saving PLS results with WriteBrik.
- Fixed error for Behavioural PLS that occurred if behavior names contained spaces in .xls file.
- Fixed masking for PLS analyses (see above).
- Fixed various text errors in PLS result figures.

- Fixed error that occurred during source group-averaging.
- Fixed error with TimeLegend files attempting to be moved into the same directory.
- Fixed issue with spaces in folder & filenames causing problems for AFNI functions.
- Fixed error that would occur during NIFTI creation when using MATLAB PCT.



============
VERSION 1.03
============

- Misc. bug fixes encountered during pipeline usage & installation.

