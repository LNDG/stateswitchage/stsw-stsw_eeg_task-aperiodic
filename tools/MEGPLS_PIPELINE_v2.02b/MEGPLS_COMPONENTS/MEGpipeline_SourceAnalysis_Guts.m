%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Performs source localization via Fieldtrip.  %
% Last modified: Jan. 15, 2014                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Usage:
%  SourceData = MEGpipeline_SourceAnalysis_Guts...
%	(FTcfg, TimeWindow, Hdm, Lead, Filter, MEGdata)
%  
% Important: If you are using this function in a command-line capacity, you
%  will need to compute pseudo-statistics manually after SourceData is computed.
%  As such, this function will NOT save the SourceData variable to disk.
%
% Output:
%  This function does NOT save the output SourceData variable to disk.
%  Instead, outputs "SourceData" variable so user can compute pseudo-statistics.
%
% Inputs:
%  Note: FTcfg can be generated & imported from a Builder .mat file.
%  FTcfg.Timelock = Fieldtrip config for ft_timelockanalysis.
%  FTcfg.Freq     = Fieldtrip config for ft_freqanalysis.
%  FTcfg.Source   = Fieldtrip config for ft_sourceanalysis.
%
%  TimeWindow = [Start, End] in seconds for source analysis.
%
%  Hdm        = Path to FT .mat headmodel file OR loaded headmodel structure.
%  Lead       = Path to FT .mat leadfield file OR loaded leadfield structure.
%  Filter     = Path to FT .mat source-filter  OR loaded source-filter structure OR empty.
%  MEGdata    = Path to FT .mat MEGdata file   OR loaded MEGdata structure.


% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function SourceData = MEGpipeline_SourceAnalysis_Guts...
	(MainFTcfg, TimeWindow, Hdm, Lead, Filter, MEGdata)


SourceData = [];  % Initialize output var


% Check number of input & output arguments:
if nargin ~= 6
    error('ERROR: Incorrect number of input arguments. See help for usage info.')
end
if nargout ~= 1
    error('ERROR: Incorrect number of output arguments. See help for usage info.')
end


% If input files are .mat paths (and NOT loaded structures):
if ~isstruct(MEGdata)
    CheckInput = CheckPipelineMat(MEGdata, 'SourceAnalysis');
    if CheckInput == 0
        return;
    end
end
if ~isstruct(Hdm)
    CheckInput = CheckPipelineMat(Hdm, 'SourceAnalysis');
    if CheckInput == 0
        return;
    end
end
if ~isstruct(Lead)
    CheckInput = CheckPipelineMat(Lead, 'SourceAnalysis');
    if CheckInput == 0
        return;
    end
end
if ~isempty(Filter) && ~isstruct(Filter)
    CheckInput = CheckPipelineMat(Filter, 'SourceAnalysis');
    if CheckInput == 0
        return;
    end
end


% Check source method:
if ismember(MainFTcfg.Source.method, {'dics', 'pcc'})
    SourceMethodDomain = 'Freq';  % DICS & PCC are for freq-domain data.
else
    SourceMethodDomain = 'Time';  % All other methods are for time-domain data.
end


%--- Redefine data to isolate for time-window: ---%
%-------------------------------------------------%

disp(['Getting time-window: ',num2str(TimeWindow(1)),'s_',num2str(TimeWindow(2)),'s'])

cfgRedefine        = [];
cfgRedefine.trials = 'all';
cfgRedefine.toilim = TimeWindow;

if ~isstruct(MEGdata)  % If .mat path specified
    cfgRedefine.inputfile = MEGdata;
    WindowData = ft_redefinetrial(cfgRedefine);
else
    WindowData = ft_redefinetrial(cfgRedefine, MEGdata);
    MEGdata    = [];  % Free memory
end


%--- Run timelock / freq analysis for source analysis input: ---%
%---------------------------------------------------------------%

switch SourceMethodDomain
	case 'Time'
		FTcfg.Timelock            = [];
		FTcfg.Timelock            = MainFTcfg.Timelock;
		FTcfg.Timelock.covariance = 'yes';
		TimeFreqData = ft_timelockanalysis(FTcfg.Timelock, WindowData)
		
	case 'Freq'
		FTcfg.Freq            = [];
		FTcfg.Freq            = MainFTcfg.Freq;
        FTcfg.Freq.output     = 'powandcsd';
		TimeFreqData = ft_freqanalysis(FTcfg.Freq, WindowData)
end


%--- Run source analysis: ---%
%----------------------------%

FTcfg.Source      = [];
FTcfg.Source      = MainFTcfg.Source;

if ~isstruct(Hdm)
    FTcfg.Source.vol = LoadFTmat(Hdm, 'SourceAnalysis');
else
    FTcfg.Source.vol = Hdm;
end

if ~isstruct(Lead)
    FTcfg.Source.grid = LoadFTmat(Lead, 'SourceAnalysis');
else
    FTcfg.Source.grid = Lead;
end

% If filter specified, load & check it:
if ~isempty(Filter)
    if ~isstruct(Filter)
        Filter = LoadFTmat(Filter, 'SourceAnalysis');
    end
    
    % Make sure method for filter calculation was the same:
    if ~isequal(Filter.cfg.method, FTcfg.Source.method)
        ErrLog = fopen('ErrorLog_SourceAnalysis.txt', 'a');
        
        fprintf(ErrLog, ['ERROR:'...
            '\n Source-filter was computed using a different source method'...
            '\n than the one currently specified for source analysis.'...
            '\n Recompute filter using the current source method to continue. \n\n']);
        
        fclose(ErrLog);
        return;
    end
    
    % Make sure lambda value for filter calculation was the same:
    CurrentLambda = [];
    FilterLambda  = [];
    
    if isfield(FTcfg.Source.(FTcfg.Source.method), 'lambda')
        CurrentLambda = FTcfg.Source.(FTcfg.Source.method).lambda;
    end
    if isfield(Filter.cfg.(FTcfg.Source.method), 'lambda')
        FilterLambda  = Filter.cfg.(FTcfg.Source.method).lambda;
    end
    
    if ~isequal(CurrentLambda, FilterLambda)
        ErrLog = fopen('ErrorLog_SourceAnalysis.txt', 'a');
        
        fprintf(ErrLog, ['ERROR:'...
            '\n Source-filter was computed using a different lambda value'...
            '\n than the one currently specified for source analysis.'...
            '\n Recompute filter using the current lambda value to continue. \n\n']);
        
        fclose(ErrLog);
        return;
    end
    
	FTcfg.Source.grid.filter = Filter.avg.filter;
    Filter = [];  % Free memory
end

disp('Running source localization:')
SourceData = ft_sourceanalysis(FTcfg.Source, TimeFreqData);


% Remove "cfg.previous", "cfg.callinfo", and "cfg.grid.filter" fields to save space!
SourceData.cfg = rmfield(SourceData.cfg, 'previous');
SourceData.cfg = rmfield(SourceData.cfg, 'callinfo');

if isfield(SourceData.cfg.grid, 'filter')
    SourceData.cfg.grid.filter = [];
    SourceData.cfg.grid.filter = 'Cleared to save space. See SOURCE_FILTER folder.';
end
