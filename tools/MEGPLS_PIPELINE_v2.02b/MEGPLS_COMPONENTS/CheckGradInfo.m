%===============================================================%
% Checks gradiometer info for Custom-Filter & Control datasets: %
% Last modified: Jan.15, 2014                                   %
%===============================================================%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function GradMismatch = CheckGradInfo(LoadedBuilderMat, RunSection)
GradErrLog = fopen(['ErrorLog_',RunSection,'.txt'], 'a');


% Load builder .mat file:
name         = LoadedBuilderMat.name;
paths        = LoadedBuilderMat.paths;
gui          = LoadedBuilderMat.gui;
PipeSettings = LoadedBuilderMat.PipeSettings;

SourceContrast     = PipeSettings.Source.Contrast;
EstNoiseCommonFilt = PipeSettings.Source.EstNoiseCommonFilt;


% Only required for Fieldtrip data:
if ~strcmp(gui.MEGdataFiletype, 'Fieldtrip') || isempty(paths.MEGdata)
    GradMismatch = 0;
    return;
end

% For DiffBase and EstNoise (No custom-filters):
% No need to check grad info since filters are computed individually.
if strcmp(SourceContrast, 'DiffBase') || ...
        strcmp(SourceContrast, 'EstNoise') && strcmp(EstNoiseCommonFilt, 'no')
    GradMismatch = 0;
    return;
end



%======================================================%
% CHECK GRADIOMETER INFO OF CUSTOM & CONTROL DATASETS: %
%======================================================%
GradMismatch = 0;

for g = 1:length(name.GroupID)
    for s = 1:length(name.SubjID{g})
        for c = 1:length(name.CondID)
            
            
            %--- Check grad. info for Common-Filter datasets: ---%
            %----------------------------------------------------%
            
            if strcmp(SourceContrast, 'EstNoise') && strcmp(EstNoiseCommonFilt, 'yes')
                MEGdataCommonFilt = LoadFTmat(paths.MEGdataCommonFilt{g}{s}, RunSection);
                if isempty(MEGdataCommonFilt)
                    continue;
                end
                
                MEGdata = LoadFTmat(paths.MEGdata{g}{s,c}, RunSection);
                if isempty(MEGdata)
                    continue;
                end
                
                if ~isequal(MEGdataCommonFilt.grad, MEGdata.grad)
                    if GradMismatch == 0
                        GradMismatch = 1;
                        
                        fprintf(GradErrLog, ['ERROR:'...
                            '\n Datasets from which common-filters are computed from do  '...
                            '\n NOT have the same gradiometer info as their respective   '...
                            '\n input datasets. Common-filters cannot be applied.        '...
                            '\n'...
                            '\n In order for common-filters to be used:                  '...
                            '\n For each subject, the common-filter dataset and its      '...
                            '\n respective input datasets must have matching gradiometer '...
                            '\n info (usually originating from the same MEG session).    '...
                            '\n'...
                            '\n The following input datasets have different gradiometer  '...
                            '\n info from their respective common-filter datasets. \n\n']);
                    end
                        
                    fprintf(GradErrLog, ' -> %s \n', paths.MEGdata{g}{s,c});
                end
            end
            
            
            %--- Check grad. info for DiffCond Control datasets: ---%
            %-------------------------------------------------------%
            
            if strcmp(SourceContrast, 'DiffCond')
                MEGdataControl = LoadFTmat(paths.MEGdataControl{g}{s}, RunSection);
                if isempty(MEGdataControl)
                    continue;
                end
                
                MEGdataActive = LoadFTmat(paths.MEGdata{g}{s,c}, RunSection);
                if isempty(MEGdataActive)
                    continue;
                end
                
                if ~isequal(MEGdataControl.grad, MEGdataActive.grad)
                    if GradMismatch == 0
                        GradMismatch = 1;
                        
                        fprintf(GradErrLog, ['ERROR:'...
                            '\n Control datasets do NOT have the same gradiometer info   '...
                            '\n as their respective active condition datasets. Control   '...
                            '\n and active conditions cannot be merged for common-filter.'...
                            '\n'...
                            '\n As a result, the DiffCond setting (vs. Control Cond)     '...
                            '\n cannot be used. Gradiometer info must match (usually     '...
                            '\n originating from the same MEG session) in order for      '...
                            '\n common-filter to be computed across active & control.    '...
                            '\n'...
                            '\n The following input datasets have different gradiometer  '...
                            '\n info from their respective control datasets. \n\n']);
                    end
                    
                    fprintf(GradErrLog, ' -> %s \n', paths.MEGdata{g}{s,c});
                end
            end
            
            
        end  % Cond
    end  % Subj
end  % Group

fclose(GradErrLog);
