%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Segments preprocessed MRI via Fieldtrip:    %
% Inputs: Resliced MRI data from Fieldtrip.   %
% Last modified: Jan. 15, 2014                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function MEGpipeline_SegmentMRI(BuilderMat)


% Make sure java paths added:
UseProgBar = CheckParforJavaPaths;

% Load builder .mat file:
Builder		 = load(BuilderMat);
name		 = Builder.name;
paths		 = Builder.paths;
FTcfg        = Builder.FTcfg;
PipeSettings = Builder.PipeSettings;

% Clear existing Errorlog & Diary:
if exist('ErrorLog_SegmentMRI.txt', 'file')
    system('rm ErrorLog_SegmentMRI.txt');
end
if exist('Diary_SegmentMRI.txt', 'file')
    system('rm Diary_SegmentMRI.txt');
end

diary Diary_SegmentMRI.txt
ErrLog = fopen('ErrorLog_SegmentMRI.txt', 'a');



%=====================================%
% SEGMENT PREPROCESSED FIELDTRIP MRI: %
%=====================================%

for g = 1:length(name.GroupID)
    NumSubj = length(name.SubjID{g});
    
    if UseProgBar == 1
        ppm = ParforProgMon(['SEGMENTING MRI FILES:  ',name.GroupID{g},'.  '], NumSubj, 1, 700, 80);
    end
    
	parfor s = 1:length(name.SubjID{g})
        CheckInput = CheckPipelineMat(paths.MRIdata{g}{s}, 'SegmentMRI');
        if CheckInput == 0
            continue;
        end
        
        CheckSavePerms(paths.SegMRI{g}{s}, 'SegmentMRI');
		
		cfgSegMRI                   = [];
		cfgSegMRI                   = FTcfg.SegMRI;
        cfgSegMRI.inputfile         = paths.MRIdata{g}{s};
        cfgSegMRI.outputfile        = paths.SegMRI{g}{s};
        cfgSegMRI.outputfilepresent = 'overwrite';
        
        disp('Segmenting MRI:')
		ft_volumesegment(cfgSegMRI)
        
        if UseProgBar == 1 && mod(s, 1) == 0
            ppm.increment();  % move up progress bar
        end
        
	end  % Subj
    if UseProgBar == 1
        ppm.delete();
    end
    
end  % Group



%=========================%
% CHECK FOR OUTPUT FILES: %
%=========================%

for g = 1:length(name.GroupID)
	for s = 1:length(name.SubjID{g})
		
		if ~exist(paths.SegMRI{g}{s}, 'file')
			fprintf(ErrLog, ['ERROR: Output segmented MRI file missing:'...
				'\n %s \n\n'], paths.SegMRI{g}{s});
		end
		
	end
end



%=================%

if exist([pwd,'/ErrorLog_SegmentMRI.txt'], 'file')
    LogCheck = dir('ErrorLog_SegmentMRI.txt');
    if LogCheck.bytes ~= 0  % File not empty
        open('ErrorLog_SegmentMRI.txt');
    else
        delete('ErrorLog_SegmentMRI.txt');
    end
end

fclose(ErrLog);
diary off

end
