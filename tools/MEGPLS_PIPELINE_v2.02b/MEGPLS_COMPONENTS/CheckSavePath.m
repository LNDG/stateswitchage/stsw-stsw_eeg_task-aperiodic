%================================================%
% Prepares and checks for file-save in pipeline: %
% Last modified: Jan.15, 2014                    %
%================================================%
%
% NOTE: Currently set to hard-error in pipeline if any problems detected.
% - To continue despite save errors, comment out "error(ErrMsg)" lines.


% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function CheckSavePath(OutputFullpath, ErrLogName)
SaveErrLog = fopen(['ErrorLog_',ErrLogName,'.txt'], 'a');


% Check if directory exists:
[OutFolder, ~, ~] = fileparts(OutputFullpath);

if ~exist(OutFolder, 'dir')
    [Status, ErrMsg] = mkdir(OutFolder);
    
    if Status == 0
        ErrMsg = sprintf(['ERROR: Failed to create output directory:'...
            '\n Reason: %s \n Folder: %s \n\n'], ErrMsg, OutFolder);
        
        fprintf(SaveErrLog, ErrMsg);  % Record in ErrorLog
        error(ErrMsg);
    end
end


% Check write permissions on directory:
[~, DirAttrib] = fileattrib(OutFolder);

if DirAttrib.UserWrite ~= 1
    ErrMsg = sprintf(['ERROR: Do not have write permissions for output directory:'...
        '\n %s \n\n'], OutFolder);
    
    fprintf(SaveErrLog, ErrMsg);  % Record in ErrorLog
    error(ErrMsg);
end


% Check for existing file, and remove for overwrite:
if exist(OutputFullpath, 'file')
    delete(OutputFullpath);
    
    [Folder, File, Ext] = fileparts(OutputFullpath);
    if strcmp(Ext, '.BRIK');  % If .BRIK file, also remove .HEAD
        delete([Folder,'/',File,'.HEAD']);
    end
    
    if exist(OutputFullpath, 'file')
        WarnMsg = sprintf(['WARNING: Could not remove existing file for overwrite.'...
            '\n %s \n\n'], OutputFullpath);
        
        fprintf(SaveErrLog, WarnMsg);  % Record in ErrorLog
        disp(WarnMsg);
    end
end 

fclose(SaveErrLog);
