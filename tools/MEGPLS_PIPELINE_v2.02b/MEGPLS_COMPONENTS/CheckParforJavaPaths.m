%=============================================%
% Make sure each worker has java paths added. %
% Note: Required for parfor progress bar.     %
% Last modified: Sept. 7, 2014                %
%=============================================%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function Success = CheckParforJavaPaths


CurrentRunDir = pwd;

[PipelineDir, ~, ~] = fileparts(which('DriverGUI.m'));
JavaFolder = [PipelineDir,'/MEGPLS_COMPONENTS/MatlabFileExchange/parforprogress/java'];

if ~exist(JavaFolder, 'dir')
    message = {'ERROR:'; '';
        'Could not find java functions folder for the parfor progress bar.';
        'Progress bar will be disabled.'; ''};
    msgbox(message, 'ERROR:')
    Success = 0;
    return;
end

Toolboxes = ver;

if any(strcmp('Parallel Computing Toolbox', {Toolboxes.Name}))
    if matlabpool('size') == 0
        javaaddpath(JavaFolder);
    else
        cd(JavaFolder);
        pctRunOnAll javaaddpath(pwd)
        cd(CurrentRunDir);
    end
    
else
    javaaddpath(JavaFolder);
end

Success = 1;
