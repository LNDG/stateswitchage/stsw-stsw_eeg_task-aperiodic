%=========================================%
% Loads & checks .mat files for pipeline: %
% Last modified: Jan. 15, 2014            %
%=========================================%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function OutputVarName = LoadFTmat(FieldtripMat, RunSection)
LoadErrLog = fopen(['ErrorLog_',RunSection,'.txt'], 'a');


% Check if file exists:
if ~exist(FieldtripMat, 'file')
	fprintf(LoadErrLog, 'ERROR: Input file does not exist: \n %s \n\n', FieldtripMat);
    disp('ERROR: Failed to load. File does not exist.')
    
	OutputVarName = [];
    return;
end

% Make sure file is .mat:
[~, ~, FileExt] = fileparts(FieldtripMat);

if ~strcmp(FileExt, '.mat')
    fprintf(LoadErrLog, 'ERROR: Input file is not a .mat: \n %s \n\n', FieldtripMat);
    disp('ERROR: Failed to load. File is not a .mat.')
    
    OutputVarName = [];
    return;
end


% Load file:
LoadMat = load(FieldtripMat);
GetVars = fieldnames(LoadMat);


% Check if .mat is empty:
if isempty(GetVars)
	fprintf(LoadErrLog, 'ERROR: Loaded .mat is empty: \n %s \n\n', FieldtripMat);
    disp('ERROR: Failed to load. File was empty.')
    
	OutputVarName = [];
    return;
end


% Check if .mat is FT compatible:
if length(GetVars) > 1
	fprintf(LoadErrLog, ['ERROR: Loaded .mat is not FieldTrip compatible.'...
		'\nThe .mat file should only contain a single variable:'...
		'\n %s \n\n'], FieldtripMat);
    disp('ERROR: Failed to load. Input .mat file is not FieldTrip compatible.')
    
	OutputVarName = [];
    return;
end


% Set output variable:
OutputVarName = getfield(LoadMat, GetVars{1});
fclose(LoadErrLog);