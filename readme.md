[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

## Aperiodic (1/f) slopes

The aperiodic 1/f slope of neural recordings is closely related to the sample entropy of broadband signals 17 and has been suggested as a proxy for ‘cortical excitability’ and excitation-inhibition balance 8. Spectral estimates were computed by means of a Fast Fourier Transform (FFT) over the final 2.5 s of the presentation period (to exclude onset transients) for 41 logarithmically spaced frequencies between 2 and 64 Hz (Hanning-tapered segments zero-padded to 10 s) and subsequently averaged. Spectral power was log10-transformed to render power values more normally distributed across participants. Power spectral density (PSD) slopes were derived by linearly regressing log10-transformed power values on log10-transformed frequencies. The spectral range from 7-13 Hz was excluded from the background fit to exclude a bias by the narrowband alpha peak 17 and thus to increase the specificity to aperiodic variance. Linear load effects on 1/f slopes were assessed by univariate cluster-based permutation tests on channel data (see Univariate statistical analyses using cluster-based permutation tests).

**a_calculateSpectra**

- CSD transform
- TFR transform: final 2.5 s of stimulus presentation (skipping initial 500 ms)

**b_plotSpectra**

- linear fits between 1 and 64 Hz, excluding 8-15 and 28-38 Hz range (log10-log10 fits)
- outputs: `C_SlopeFits_v3_OA.mat`
- CBPA of linear changes with load
- important: spectra are now similarly plotted at occipital channels (O1,Oz,O2)

Topography YA:
![image](figures/b_1_f_topography_YA.png)

Topography OA:
![image](figures/b_1_f_topography_OA.png)

Spectrum at loads 1 and 4 YA:
![image](figures/b_1_f_rotation_v3_YA.png)

Spectrum at loads 1 and 4 OA:
![image](figures/b_1_f_rotation_v3_OA.png)

**c_plotLinearLoad**

- refers to results in summary structure
- plot raincloudplots after accounting for outliers larger than 2.5 times Cook's distance

Younger adults:
![image](figures/c_1_f_modulation_YA.png)

Older adults:
![image](figures/c_1_f_modulation_OA.png)

**d_plotComparativeSpectra**

Estimates at occipital channels:
![image](figures/d_1f_YAOA_extracted.png)

Based on this, it looks like OA may be overexcited already at load 1, with little additional modulation. Note that there is no pairwise difference between groups however.

**e01_taskPLS_load_yaoa**

- 2 group task PLS with only posterior-occipital channels

Topography:
![image](figures/e01_pls_topo.png)

RCP, outlier with Cook's distance > 2.5 removed from stats:
![image](figures/e01_pls_rcp.png)