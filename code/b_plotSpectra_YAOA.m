%% set paths

restoredefaultpath;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data         = fullfile(rootpath, 'data');
pn.dataOut      = fullfile(rootpath, 'data', 'aperiodic_out');
pn.figures      = fullfile(rootpath, 'figures');
pn.tools        = fullfile(rootpath, 'tools');
    addpath(pn.tools);
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
    addpath(fullfile(pn.tools, 'BrewerMap'));
    addpath(fullfile(pn.tools, 'shadedErrorBar'));
    addpath(fullfile(pn.tools, 'barwitherr'));
    addpath(fullfile(pn.tools, 'RainCloudPlots'));
pn.summary	= fullfile(rootpath, '..', '..', 'stsw_multimodal', 'data');

%% load PSD data

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

MergedPow = [];
for id = 1:length(IDs)
    load(fullfile(pn.dataOut, [IDs{id}, '_1f.mat']), 'freq');
    MergedPow(id,:,:,:) = freq.powspctrm;
end

%% linear fit between 1-64 Hz (excluding 8-15 Hz range, 28-38 Hz)
% shallower slope --> increasing neural irregularity

nonAlphaSub30 = find(freq.freq <8 | freq.freq >15 & freq.freq <28 | freq.freq >32);

linFit_2_30 = [];
for indID = 1:size(MergedPow,1)
    for indCond = 1:4
        for indChan = 1:60
            x = log10(freq.freq(nonAlphaSub30))';
            y = log10(squeeze(MergedPow(indID,indCond,indChan,nonAlphaSub30)));
            pv=polyfit(x,y,1); % linear regression
            linFit_2_30(indID, indCond, indChan) = pv(1);
        end
    end
end

SlopeFits.IDs = IDs;
SlopeFits.linFit_2_30 = linFit_2_30;

save(fullfile(pn.data, 'C_SlopeFits_YAOA.mat'), 'SlopeFits')
load(fullfile(pn.data, 'C_SlopeFits_YAOA.mat'), 'SlopeFits')

%% CBPA
    
    StatStruct = [];
    for indCond = 1:4
        StatStruct{indCond} = freq;
        StatStruct{indCond}.freq = 1;
        StatStruct{indCond}.powspctrm = squeeze(linFit_2_30(:,indCond,:));
        StatStruct{indCond}.dimord = 'subj_chan';
    end

    % prepare_neighbours determines what sensors may form clusters
    cfg_neighb.method       = 'template';
    cfg_neighb.template     = 'elec1010_neighb.mat';
    cfg_neighb.channel      = StatStruct{1}.label;

    cfgStat = [];
    cfgStat.channel          = 'all';
    cfgStat.method           = 'montecarlo';
    cfgStat.statistic        = 'ft_statfun_depsamplesregrT';
    cfgStat.correctm         = 'cluster';
    cfgStat.clusteralpha     = 0.05;
    cfgStat.clusterstatistic = 'maxsum';
    %cfgStat.minnbchan        = 2;
    cfgStat.tail             = 0;
    cfgStat.clustertail      = 0;
    cfgStat.alpha            = 0.05;
    cfgStat.numrandomization = 1500;
    cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, StatStruct{1,1,1});

    subj = size(StatStruct{indCond}.powspctrm,1);
    conds = 4;
    design = zeros(2,conds*subj);
    for indCond = 1:conds
    for i = 1:subj
        design(1,(indCond-1)*subj+i) = indCond;
        design(2,(indCond-1)*subj+i) = i;
    end
    end
    cfgStat.design   = design;
    cfgStat.ivar     = 1;
    cfgStat.uvar     = 2;

    stat = [];
    cfgStat.parameter = 'powspctrm';
    [stat] = ft_freqstatistics(cfgStat, StatStruct{1}, StatStruct{2}, StatStruct{3}, StatStruct{4});

    save(fullfile(pn.data, 'C_stat_YAOA.mat'), 'stat')
    load(fullfile(pn.data, 'C_stat_YAOA.mat'), 'stat')
    
%% plot topography

    cBrew = brewermap(500,'RdBu');
    cBrew = flipud(cBrew);

    %figure; imagesc(stat.stat.*stat.mask)

    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'EastOutside';
    cfg.marker = 'off';  
    cfg.highlight = 'yes';
    cfg.highlightchannel = stat.label(stat.mask);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 40;
    cfg.zlim = [-5 5];
    cfg.style = 'both';
    cfg.colormap = cBrew;

    h = figure('units','normalized','position',[.1 .1 .2 .2]); 
    plotData = [];
    plotData.label = stat.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(stat.stat);
    ft_topoplotER(cfg,plotData);
    cb = colorbar(cfg.colorbar); set(get(cb,'title'),'string','t values');
    pval = []; pval = convertPtoExponential(stat.posclusters(1).prob);
    title({'Linear effect 1/f slopes', ['p = ', pval{1}]});
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
figureName = 'b_1_f_topography_YAOA';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% plot 1/f rotation

    % IMPORTANT: TMP TO REPRODUCE YA
    stat.posclusterslabelmat = zeros(60,1);
    stat.posclusterslabelmat(58:60) = 1;

    % shading presents within-subject standard errors
    % new value = old value ??? subject average + grand average

    cBrew = brewermap(4,'RdBu');
    cBrew = flipud(cBrew);
    h = figure('units','normalized','position',[.1 .1 .2 .2]); hold on;
    condAvg = squeeze(nanmean(nanmean(log10(MergedPow(:,:,find(stat.posclusterslabelmat),:)),3),2));
    curData = squeeze(nanmean(log10(MergedPow(:,1,find(stat.posclusterslabelmat),:)),3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(nanmean(log10(MergedPow(:,4,find(stat.posclusterslabelmat),:)),3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
    xlim([0 1.9]); ylim([-10 -6.5])
    xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');
    legend([l1.mainLine, l4.mainLine], {'1 Target', '4 Targets'}, 'location', 'SouthEast'); legend('boxoff')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

    % add emphasis of effect ranges
    
    handaxes1 = axes('Position', [0.22 0.3 0.2 .2]); cla;
    hold on;
    condAvg = squeeze(nanmean(nanmean(log10(MergedPow(:,:,find(stat.posclusterslabelmat),:)),3),2));
    curData = squeeze(nanmean(log10(MergedPow(:,1,find(stat.posclusterslabelmat),:)),3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 1}, 'patchSaturation', .25);
    curData = squeeze(nanmean(log10(MergedPow(:,4,find(stat.posclusterslabelmat),:)),3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(4,:),'linewidth', 1}, 'patchSaturation', .25);
    xlim([0 .1]); ylim([-7.45 -7.2]) % ylim([-7.05 -6.85])
    set(findall(handaxes1,'-property','FontSize'),'FontSize',14)

    handaxes2 = axes('Position', [0.68 0.65 0.2 .2]); cla;
    hold on;
    condAvg = squeeze(nanmean(nanmean(log10(MergedPow(:,:,find(stat.posclusterslabelmat),:)),3),2));
    curData = squeeze(nanmean(log10(MergedPow(:,1,find(stat.posclusterslabelmat),:)),3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 1}, 'patchSaturation', .25);
    curData = squeeze(nanmean(log10(MergedPow(:,4,find(stat.posclusterslabelmat),:)),3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(4,:),'linewidth', 1}, 'patchSaturation', .25);
    xlim([1.75 1.9]); ylim([-9.15 -8.88])
    set(findall(handaxes2,'-property','FontSize'),'FontSize',14)

figureName = 'b_1_f_rotation_YAOA';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% plot frontal spectrum (requires 1/f output structure)

cBrew = brewermap(4,'RdBu');
cBrew = flipud(cBrew);
h = figure('units','normalized','position',[.1 .1 .125 .2]); hold on;
curData = squeeze(nanmean(nanmean(MergedPow(:,[1:4],10:12,:),3),2));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l1 = shadedErrorBar([],nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25)
%set(gca, 'XTick', 2.^[0:.125:6.5]);
xlim([7 50]); set(gca, 'XTickLabels', round(freq.freq(get(gca, 'XTick')),1));
xlabel('Frequency (Hz)'); ylabel('Power (log10; Hz)');
set(findall(gcf,'-property','FontSize'),'FontSize',18)

% cBrew = brewermap(4,'RdBu');
% cBrew = flipud(cBrew);
% h = figure('units','normalized','position',[.1 .1 .125 .2]); hold on;
% curData = squeeze(nanmean(nanmean(MergedPow(:,[1,4],10:12,:),3),2))-...
%     squeeze(nanmean(nanmean(MergedPow(:,[2,3],10:12,:),3),2));
% standError = nanstd(curData,1)./sqrt(size(curData,1));
% l1 = shadedErrorBar([],nanmean(curData,1),standError, ...
%     'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25)
% %set(gca, 'XTick', 2.^[0:.125:6.5]);
% xlim([7 50]); set(gca, 'XTickLabels', round(freq.freq(get(gca, 'XTick')),1));
% xlabel('Frequency (Hz)'); ylabel('Power (log10; Hz)');
% set(findall(gcf,'-property','FontSize'),'FontSize',18)
% % 
% pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/C_figures/';
% figureName = 'C3_taskPLS_DeltaThetaFreq';
% 
% saveas(h, [pn.plotFolder, figureName], 'epsc');
% saveas(h, [pn.plotFolder, figureName], 'png');

%% check whether there is frontal 1/f rotation

% idx_chan = 10:12;
% 
% % shading presents within-subject standard errors
% % new value = old value ??? subject average + grand average
% 
% cBrew = brewermap(4,'RdBu');
% cBrew = flipud(cBrew);
% h = figure('units','normalized','position',[.1 .1 .1 .2]); hold on;
% condAvg = squeeze(nanmean(nanmean(log10(MergedPow(:,:,idx_chan,:)),3),2));
% curData = squeeze(nanmean(nanmean(log10(MergedPow(:,[1,4],idx_chan,:)),3),2));
% curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
% standError = nanstd(curData,1)./sqrt(size(curData,1));
% l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
%     'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
% % curData = squeeze(nanmean(log10(MergedPow(:,2,idx_chan,:)),3));
% % curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
% % standError = nanstd(curData,1)./sqrt(size(curData,1));
% % l2 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
% %     'lineprops', {'color', cBrew(3,:),'linewidth', 2}, 'patchSaturation', .25);
% curData = squeeze(nanmean(nanmean(log10(MergedPow(:,[2,3],idx_chan,:)),3),2));
% curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
% standError = nanstd(curData,1)./sqrt(size(curData,1));
% l3 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
%     'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
% xlim([0 1.9]); %ylim([-10 -6.5])
% xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');
% legend([l1.mainLine, l3.mainLine], {'1 Target', '3 Targets'}, 'location', 'SouthEast'); legend('boxoff')
% set(findall(gcf,'-property','FontSize'),'FontSize',18)
% 
% cBrew = brewermap(4,'RdBu');
% cBrew = flipud(cBrew);
% h = figure('units','normalized','position',[.1 .1 .1 .2]); hold on;
% curData = squeeze(nanmean(nanmean(log10(MergedPow(:,[1,4],idx_chan,:)),3),2))-squeeze(nanmean(nanmean(log10(MergedPow(:,[2,3],idx_chan,:)),3),2));
% standError = nanstd(curData,1)./sqrt(size(curData,1));
% l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
%     'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
% 
% cBrew = brewermap(4,'RdBu');
% cBrew = flipud(cBrew);
% h = figure('units','normalized','position',[.1 .1 .1 .2]); hold on;
% curData = squeeze(nanmean(nanmean(log10(MergedPow(:,[1],idx_chan,:)),3),2))-squeeze(nanmean(nanmean(log10(MergedPow(:,[1:4],idx_chan,:)),3),2));
% standError = nanstd(curData,1)./sqrt(size(curData,1));
% l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
%     'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
% curData = squeeze(nanmean(nanmean(log10(MergedPow(:,[2],idx_chan,:)),3),2))-squeeze(nanmean(nanmean(log10(MergedPow(:,[1:4],idx_chan,:)),3),2));
% standError = nanstd(curData,1)./sqrt(size(curData,1));
% l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
%     'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
% 
% curData = squeeze(nanmean(nanmean(log10(MergedPow(:,[3],idx_chan,:)),3),2))-squeeze(nanmean(nanmean(log10(MergedPow(:,[1:4],idx_chan,:)),3),2));
% standError = nanstd(curData,1)./sqrt(size(curData,1));
% l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
%     'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
% 
% 
% curData = squeeze(nanmean(nanmean(log10(MergedPow(:,[4],idx_chan,:)),3),2))-squeeze(nanmean(nanmean(log10(MergedPow(:,[1:4],idx_chan,:)),3),2));
% standError = nanstd(curData,1)./sqrt(size(curData,1));
% l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
%     'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
% 

