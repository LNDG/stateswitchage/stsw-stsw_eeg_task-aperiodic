%% setup 

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data         = fullfile(rootpath, 'data');
pn.figures      = fullfile(rootpath, 'figures');
pn.tools        = fullfile(rootpath, 'tools');
    addpath(pn.tools);
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
    addpath(fullfile(pn.tools, 'BrewerMap'));
    addpath(fullfile(pn.tools, 'shadedErrorBar'));
    addpath(fullfile(pn.tools, 'barwitherr'));
    addpath(genpath(fullfile(pn.tools, 'RainCloudPlots')));
    addpath(fullfile(pn.tools, 'Cookdist'));
pn.summary	= fullfile(rootpath, '..', '..', 'stsw_multimodal', 'data');

%% load data

    load(fullfile(pn.summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

%% plot parametric 1/f slope modulation

    % N = 47;
    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281'};
    
    summaryIdx = ismember(STSWD_summary.IDs, IDs);  

%% plot RainCloudPlot

% define outlier as lin. modulation of 2.5*mean Cook's distance
cooks = Cookdist(STSWD_summary.OneFslope.data(summaryIdx,:));
outliers = cooks>2.5*mean(cooks);
idx_outlier = find(outliers);
idx_standard = find(outliers==0);

% read into cell array of the appropriate dimensions
data = []; data_ws = [];
for i = 1:4
    for j = 1:1
        data{i, j} = STSWD_summary.OneFslope.data(summaryIdx,i);
        % individually demean for within-subject visualization
        data_ws{i, j} = STSWD_summary.OneFslope.data(summaryIdx,i)-...
            nanmean(STSWD_summary.OneFslope.data(summaryIdx,:),2)+...
            repmat(nanmean(nanmean(STSWD_summary.OneFslope.data(summaryIdx,:),2),1),size(STSWD_summary.OneFslope.data(summaryIdx,:),1),1);
        data_nooutlier{i, j} = data{i, j};
        data_nooutlier{i, j}(idx_outlier) = [];
        data_ws_nooutlier{i, j} = data_ws{i, j};
        data_ws_nooutlier{i, j}(idx_outlier) = [];
        % sort outliers to back in original data for improved plot overlap
        data_ws{i, j} = [data_ws{i, j}(idx_standard); data_ws{i, j}(idx_outlier)];
    end
end

% IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

cl = [.6 .2 .2];

h = figure('units','normalized','position',[.1 .1 .2 .3]);
set(gcf,'renderer','Painters')
    cla;
    rm_raincloud_fixedSpacing(data_ws, [.8 .8 .8],1,[],[],[],15);
    h_rc = rm_raincloud_fixedSpacing(data_ws_nooutlier, cl,1,[],[],[],15);
    % add stats
%     condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
%     condPairsLevel = [-.0275, -.028, -.029, -.028, -.0283, -.029]+.006;
    condPairs = [3,4];
    condPairsLevel = [-1.1];
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(data_nooutlier{condPairs(indPair,1), j}, data_nooutlier{condPairs(indPair,2), j}); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
           mysigstar_vert(gca, [condPairsLevel(indPair), condPairsLevel(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

        end
    end
    view([90 -90]);
    axis ij
box(gca,'off')
%set(gca, 'YTick', [1,2,3,4]);
set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
ylabel('Target load'); xlabel({'PSD Slope'; '[Individually centered]'})

% test linear effect
curData = [data_nooutlier{1, 1}, data_nooutlier{2, 1}, data_nooutlier{3, 1}, data_nooutlier{4, 1}];
X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
[~, p, ci, stats] = ttest(IndividualSlopes);
title({'1/f slope modulation'; ['linear effect: p = ', num2str(round(p,3))]}); 
set(findall(gcf,'-property','FontSize'),'FontSize',30)
xlim([-1.15 -.8]); yticks = get(gca, 'ytick'); ylim([yticks(1)-(yticks(2)-yticks(1))./1.5, yticks(4)+(yticks(2)-yticks(1))./1.5]);

figureName = 'c_1_f_modulation_YA';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% SourceData

SourceData = STSWD_summary.OneFslope.data(summaryIdx,:);
SourceData_woOutlier = SourceData(outliers==0,:);