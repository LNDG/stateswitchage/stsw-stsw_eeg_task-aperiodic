%% setup 

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data         = fullfile(rootpath, 'data');
pn.figures      = fullfile(rootpath, 'figures');
pn.tools        = fullfile(rootpath, 'tools');
    addpath(pn.tools);
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
    addpath(fullfile(pn.tools, 'BrewerMap'));
    addpath(fullfile(pn.tools, 'shadedErrorBar'));
    addpath(fullfile(pn.tools, 'barwitherr'));
    addpath(genpath(fullfile(pn.tools, 'RainCloudPlots')));
    addpath(fullfile(pn.tools, 'Cookdist'));
pn.summary	= fullfile(rootpath, '..', '..', 'stsw_multimodal', 'data');

%% load data

    load(fullfile(pn.summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

%% plot parametric 1/f slope modulation

    IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};
    
    summaryIdx = ismember(STSWD_summary.IDs, IDs);  

%% plot RainCloudPlot

% define outlier as lin. modulation of 2.5*mean Cook's distance
cooks = Cookdist(STSWD_summary.OneFslope.data(summaryIdx,:));
outliers = cooks>2.5*mean(cooks);
idx_outlier = find(outliers);
idx_standard = find(outliers==0);

% read into cell array of the appropriate dimensions
data = []; data_ws = [];
for i = 1:4
    for j = 1:1
        data{i, j} = STSWD_summary.OneFslope.data(summaryIdx,i);
        % individually demean for within-subject visualization
        data_ws{i, j} = STSWD_summary.OneFslope.data(summaryIdx,i)-...
            nanmean(STSWD_summary.OneFslope.data(summaryIdx,:),2)+...
            repmat(nanmean(nanmean(STSWD_summary.OneFslope.data(summaryIdx,:),2),1),size(STSWD_summary.OneFslope.data(summaryIdx,:),1),1);
        data_nooutlier{i, j} = data{i, j};
        data_nooutlier{i, j}(idx_outlier) = [];
        data_ws_nooutlier{i, j} = data_ws{i, j};
        data_ws_nooutlier{i, j}(idx_outlier) = [];
        % sort outliers to back in original data for improved plot overlap
        data_ws{i, j} = [data_ws{i, j}(idx_standard); data_ws{i, j}(idx_outlier)];
    end
end

% IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

cl = [.6 .2 .2];

h = figure('units','normalized','position',[.1 .1 .2 .3]);
set(gcf,'renderer','Painters')
    cla;
    rm_raincloud_fixedSpacing(data_ws, [.8 .8 .8],1,[],[],[],15);
    h_rc = rm_raincloud_fixedSpacing(data_ws_nooutlier, cl,1,[],[],[],15);
    condPairs = [3,4];
    condPairsLevel = [-1.1];
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(data_nooutlier{condPairs(indPair,1), j}, data_nooutlier{condPairs(indPair,2), j}); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
           mysigstar_vert(gca, [condPairsLevel(indPair), condPairsLevel(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

        end
    end
    view([90 -90]);
    axis ij
box(gca,'off')
%set(gca, 'YTick', [1,2,3,4]);
set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
ylabel('Target load'); xlabel({'PSD Slope'; '[Individually centered]'})

% test linear effect
curData = [data_nooutlier{1, 1}, data_nooutlier{2, 1}, data_nooutlier{3, 1}, data_nooutlier{4, 1}];
X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
[~, p, ci, stats] = ttest(IndividualSlopes);
title({'1/f slope modulation'; ['linear effect: p = ', num2str(round(p,3))]}); 
set(findall(gcf,'-property','FontSize'),'FontSize',30)
xlim([-1.15 -.8]); yticks = get(gca, 'ytick'); ylim([yticks(1)-(yticks(2)-yticks(1))./1.5, yticks(4)+(yticks(2)-yticks(1))./1.5]);

figureName = 'c_1_f_modulation_OA';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% SourceData

SourceData = STSWD_summary.OneFslope.data(summaryIdx,:);
SourceData_woOutlier = SourceData(outliers==0,:);