%% set paths

restoredefaultpath;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data         = fullfile(rootpath, 'data');
pn.dataOut      = fullfile(rootpath, 'data', 'aperiodic_out');
pn.figures      = fullfile(rootpath, 'figures');
pn.tools        = fullfile(rootpath, 'tools');
    addpath(pn.tools);
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
    addpath(fullfile(pn.tools, 'BrewerMap'));
    addpath(fullfile(pn.tools, 'shadedErrorBar'));
    addpath(fullfile(pn.tools, 'barwitherr'));
    addpath(fullfile(pn.tools, 'RainCloudPlots'));
pn.summary	= fullfile(rootpath, '..', '..', 'stsw_multimodal', 'data');

%% load PSD data

IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

MergedPow = [];
for id = 1:length(IDs)
    load(fullfile(pn.dataOut, [IDs{id}, '_1f.mat']), 'freq');
    MergedPow(id,:,:,:) = freq.powspctrm;
end

%% plot spectra in full form

figure; imagesc(squeeze(nanmean(MergedPow(:,4,:,:)-MergedPow(:,1,:,:),1)))

%% linear fit between 1-64 Hz (excluding 8-15 Hz range, 28-38 Hz)
% shallower slope --> increasing neural irregularity

nonAlphaSub30 = find(freq.freq <8 | freq.freq >15 & freq.freq <28 | freq.freq >32);

linFit_2_30 = [];
for indID = 1:size(MergedPow,1)
    for indCond = 1:4
        for indChan = 1:60
            x = log10(freq.freq(nonAlphaSub30))';
            y = log10(squeeze(MergedPow(indID,indCond,indChan,nonAlphaSub30)));
            pv=polyfit(x,y,1); % linear regression
            linFit_2_30(indID, indCond, indChan) = pv(1);
        end
    end
end

SlopeFits.IDs = IDs;
SlopeFits.linFit_2_30 = linFit_2_30;

save(fullfile(pn.data, 'C_SlopeFits_v3_OA.mat'), 'SlopeFits')
load(fullfile(pn.data, 'C_SlopeFits_v3_OA.mat'), 'SlopeFits')

%% topography of slope fits

% 
% cfg = [];
% cfg.layout = 'acticap-64ch-standard2.mat';
% cfg.parameter = 'powspctrm';
% cfg.comment = 'no';
% cfg.colorbar = 'SouthOutside';
% cfg.zlim = [-.03 .03];
% 
% figure;
% subplot(2,2,1)
%     plotData = [];
%     plotData.label = freq.label; % {1 x N}
%     plotData.dimord = 'chan';
%     plotData.powspctrm = squeeze(nanmean(linFit_2_30(:,1,:),1));
%     ft_topoplotER(cfg,plotData);
%     title('Slopes EC, YA')
% subplot(2,2,2)
%     plotData = [];
%     plotData.label = freq.label; % {1 x N}
%     plotData.dimord = 'chan';
%     plotData.powspctrm = squeeze(nanmean(linFit_2_30(:,4,:),1));
%     ft_topoplotER(cfg,plotData);
%     title('Slopes EO, YA')
% subplot(2,2,3)
%     plotData = [];
%     plotData.label = freq.label; % {1 x N}
%     plotData.dimord = 'chan';
%     plotData.powspctrm = squeeze(nanmean(linFit_2_30(:,4,:),1))-squeeze(nanmean(linFit_2_30(:,1,:),1));
%     ft_topoplotER(cfg,plotData);
%     title('Slopes EC, OA')
% 
    
%% CBPA
    
StatStruct = [];
for indCond = 1:4
    StatStruct{indCond} = freq;
    StatStruct{indCond}.freq = 1;
    StatStruct{indCond}.powspctrm = squeeze(linFit_2_30(:,indCond,:));
    StatStruct{indCond}.dimord = 'subj_chan';
end

% prepare_neighbours determines what sensors may form clusters
cfg_neighb.method       = 'template';
cfg_neighb.template     = 'elec1010_neighb.mat';
cfg_neighb.channel      = StatStruct{1}.label;

cfgStat = [];
cfgStat.channel          = 'all';
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_depsamplesregrT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
%cfgStat.minnbchan        = 2;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.05;
cfgStat.numrandomization = 1500;
cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, StatStruct{1,1,1});

subj = size(StatStruct{indCond}.powspctrm,1);
conds = 4;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

stat = [];
cfgStat.parameter = 'powspctrm';
[stat] = ft_freqstatistics(cfgStat, StatStruct{1}, StatStruct{2}, StatStruct{3}, StatStruct{4});

save(fullfile(pn.data, 'C_stat_v3_OA.mat'), 'stat')
load(fullfile(pn.data, 'C_stat_v3_OA.mat'), 'stat')

% constrain to occipital cluster
stat.mask(stat.posclusterslabelmat~=2) = 0;

% IMPORTANT: we do not report the effect over motor channels here to focus
% on occipital activity
    
%% plot topography

cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);

%figure; imagesc(stat.stat.*stat.mask)

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'EastOutside';
cfg.marker = 'off';  
cfg.highlight = 'yes';
cfg.highlightchannel = stat.label(stat.mask);
cfg.highlightcolor = [0 0 0];
cfg.highlightsymbol = '.';
cfg.highlightsize = 40;
cfg.zlim = [-5 5];
cfg.style = 'both';
cfg.colormap = cBrew;

h = figure('units','normalized','position',[.1 .1 .2 .2]); 
plotData = [];
plotData.label = stat.label; % {1 x N}
plotData.dimord = 'chan';
plotData.powspctrm = squeeze(stat.stat);
ft_topoplotER(cfg,plotData);
cb = colorbar(cfg.colorbar); set(get(cb,'title'),'string','t values');
% pval = []; pval = convertPtoExponential(stat.posclusters(2).prob);
% title({'Linear effect 1/f slopes', ['p = ', pval{1}]});
set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
figureName = 'b_1_f_topography_OA';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% plot 1/f rotation

% select only posterior-occipital channels
stat.posclusterslabelmat(1:43) = 0;

% shading presents within-subject standard errors
% new value = old value ??? subject average + grand average

% IMPORTANT: TMP TO REPRODUCE YA
stat.posclusterslabelmat = zeros(60,1);
stat.posclusterslabelmat(58:60) = 1;

cBrew = brewermap(4,'RdBu');
cBrew = flipud(cBrew);
h = figure('units','normalized','position',[.1 .1 .2 .2]); hold on;
condAvg = squeeze(nanmean(nanmean(log10(MergedPow(:,:,find(stat.posclusterslabelmat),:)),3),2));
curData = squeeze(nanmean(log10(MergedPow(:,1,find(stat.posclusterslabelmat),:)),3));
curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
standError = nanstd(curData,1)./sqrt(size(curData,1));
l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
curData = squeeze(nanmean(log10(MergedPow(:,4,find(stat.posclusterslabelmat),:)),3));
curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
standError = nanstd(curData,1)./sqrt(size(curData,1));
l4 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
xlim([0 1.9]); ylim([-10 -6.5])
xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');
legend([l1.mainLine, l4.mainLine], {'1 Target', '4 Targets'}, 'location', 'SouthEast'); legend('boxoff')
%title('Divided attention induces 1/f rotation')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

% add emphasis of effect ranges

handaxes1 = axes('Position', [0.22 0.3 0.2 .2]); cla;
hold on;
condAvg = squeeze(nanmean(nanmean(log10(MergedPow(:,:,find(stat.posclusterslabelmat),:)),3),2));
curData = squeeze(nanmean(log10(MergedPow(:,1,find(stat.posclusterslabelmat),:)),3));
curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
standError = nanstd(curData,1)./sqrt(size(curData,1));
l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(1,:),'linewidth', 1}, 'patchSaturation', .25);
curData = squeeze(nanmean(log10(MergedPow(:,4,find(stat.posclusterslabelmat),:)),3));
curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
standError = nanstd(curData,1)./sqrt(size(curData,1));
l4 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(4,:),'linewidth', 1}, 'patchSaturation', .25);
xlim([0 .1]); ylim([-7.37 -7.2]) % ylim([-7.05 -6.85])
set(findall(handaxes1,'-property','FontSize'),'FontSize',14)

handaxes2 = axes('Position', [0.68 0.65 0.2 .2]); cla;
hold on;
condAvg = squeeze(nanmean(nanmean(log10(MergedPow(:,:,find(stat.posclusterslabelmat),:)),3),2));
curData = squeeze(nanmean(log10(MergedPow(:,1,find(stat.posclusterslabelmat),:)),3));
curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
standError = nanstd(curData,1)./sqrt(size(curData,1));
l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(1,:),'linewidth', 1}, 'patchSaturation', .25);
curData = squeeze(nanmean(log10(MergedPow(:,4,find(stat.posclusterslabelmat),:)),3));
curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
standError = nanstd(curData,1)./sqrt(size(curData,1));
l4 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(4,:),'linewidth', 1}, 'patchSaturation', .25);
xlim([1.75 1.9]); ylim([-9.1 -8.95])
set(findall(handaxes2,'-property','FontSize'),'FontSize',14)

figureName = 'b_1_f_rotation_v3_OA';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% plot 1/f slopes as a function of drift rates in load 1

load(fullfile(pn.summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

idxOA = ismember(STSWD_summary.IDs, IDs);
drift = squeeze(nanmean(STSWD_summary.HDDM_vat.driftEEG(idxOA,1),2));
[driftIdx, sortIdx] = sort(drift, 'descend');
highdrift = sortIdx(1:ceil(numel(sortIdx)/2));
lowdrift = sortIdx(ceil(numel(sortIdx)/2)+1:numel(sortIdx));
idxChan = 58:60;

cBrew = brewermap(4,'RdBu');
cBrew = flipud(cBrew);
h = figure('units','normalized','position',[.1 .1 .2 .2]); hold on;
curData = squeeze(nanmean(nanmean(log10(MergedPow(lowdrift,1,idxChan,:)),3),2));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
curData = squeeze(nanmean(nanmean(log10(MergedPow(highdrift,1,idxChan,:)),3),2));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l4 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
xlim([0 1.9]); ylim([-10 -6.5])
xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');
legend([l1.mainLine, l4.mainLine], {'Low drift', 'High drift'}, 'location', 'SouthEast'); legend('boxoff')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% continuous correlation within OAs

x = STSWD_summary.HDDM_vat.driftEEG(idxOA,1);
y = squeeze(nanmean(SlopeFits.linFit_2_30(:,1,58:60),3));
figure; scatter(x,y, 'filled');
[r,p] = corrcoef(x,y)
