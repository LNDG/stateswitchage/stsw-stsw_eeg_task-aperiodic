%% set paths

restoredefaultpath;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data         = fullfile(rootpath, 'data');
pn.dataOut      = fullfile(rootpath, 'data', 'aperiodic_out');
pn.figures      = fullfile(rootpath, 'figures');
pn.tools        = fullfile(rootpath, 'tools');
    addpath(pn.tools);
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
    addpath(fullfile(pn.tools, 'BrewerMap'));
    addpath(fullfile(pn.tools, 'shadedErrorBar'));
    addpath(fullfile(pn.tools, 'barwitherr'));
    addpath(fullfile(pn.tools, 'RainCloudPlots'));
pn.summary	= fullfile(rootpath, '..', '..', 'stsw_multimodal', 'data');
pn.rest	= fullfile(rootpath, '..', '..', 'stsw_eeg_rest', 'fft', 'data');

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

MergedPow = [];
for id = 1:length(IDs)
    load(fullfile(pn.dataOut, [IDs{id}, '_1f.mat']), 'freq');
    MergedPow(id,:,:,:) = freq.powspctrm;
end

load(fullfile(pn.summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')
    
selectChans = 58:60;

%% plot YA & OA spectra superimposed

cBrew = brewermap(4,'RdBu');
%cBrew = flipud(cBrew);
h = figure('units','normalized','position',[.1 .1 .15 .2]); hold on;
curData = log10(squeeze(nanmean(nanmean(MergedPow(1:47,:,selectChans,:),3),2)));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
curData = log10(squeeze(nanmean(nanmean(MergedPow(48:end,:,selectChans,:),3),2)));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l4 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
xlim([0 1.9]); ylim([-10 -6.5])
xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');
legend([l1.mainLine, l4.mainLine], {'YA', 'OA'}, 'location', 'SouthEast'); legend('boxoff')
title('No alpha for OAs, no spectral rotation')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

figureName = 'D_1f_YAOA_L1';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% add resting state spectra

IDs_YA = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';...
    '1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';...
    '1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

IDs_OA = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

load(fullfile(pn.rest, 'B_FFT_grandavg_individual.mat'))

%     figure; 
%     subplot(1,2,1); imagesc(squeeze(nanmean(grandavg_EC.powspctrm,2))); title('Eyes Closed');
%     subplot(1,2,2); imagesc(squeeze(nanmean(grandavg_EO.powspctrm,2))); title('Eyes Open');

commonYA = info.IDs_all(ismember(info.IDs_all, IDs_YA));
commonOA = info.IDs_all(ismember(info.IDs_all, IDs_OA));

idx_YA_fft = ismember(info.IDs_all, commonYA);
idx_OA_fft = ismember(info.IDs_all, commonOA);

cBrew = brewermap(4,'RdBu');
%cBrew = flipud(cBrew);
h = figure('units','normalized','position',[.1 .1 .15 .2]); hold on;
curData = log10(squeeze(nanmean(grandavg_EO.powspctrm(idx_YA_fft,selectChans,:),2)));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l1 = shadedErrorBar(log10(grandavg_EO.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
curData = log10(squeeze(nanmean(grandavg_EO.powspctrm(idx_OA_fft,selectChans,:),2)));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l4 = shadedErrorBar(log10(grandavg_EO.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
xlim([0.3 1.8]); ylim([-9.5 -7.5])
xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');
legend([l1.mainLine, l4.mainLine], {'YA', 'OA'}, 'location', 'SouthWest'); legend('boxoff')
title('Age differences at rest (EO)')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

figureName = 'D_1f_YAOA_rest';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% relate baseline slope to behavior & slope modulation (partial corr cont. age)

% load fitted log-log slopes
load(fullfile(pn.rest, 'D_PSDslopes_loglog.mat'), ...
    'linFit_2_30_EC', 'linFit_2_30_EC_int', ...
    'linFit_2_30_EO','linFit_2_30_EO_int', ...
    'grandavg_EC', 'grandavg_EO');

idx_YA_summary = ismember(STSWD_summary.IDs, commonYA);
idx_OA_summary = ismember(STSWD_summary.IDs, commonOA);

% SSVEP magnitude is strongly related to all stable 1/f slope
x = squeeze(nanmean(STSWD_summary.SSVEPmag.data_norm(idx_YA_summary,1:4),2));
y = squeeze(nanmean(STSWD_summary.OneFslope.data(logical(idx_YA_summary), 1:4),2));
%y = squeeze(nanmean(linFit_2_30_EO(idx_YA_fft,selectChans),2));
figure; scatter(x,y, 'filled')

% reliability of slope from rest to task
x = squeeze(nanmean(STSWD_summary.OneFslope.data(logical(idx_YA_summary+idx_OA_summary), 1:4),2));
linFitRest = squeeze(nanmean(cat(3,linFit_2_30_EC, linFit_2_30_EO),3));
y = squeeze(nanmean(linFitRest(logical([idx_YA_fft+idx_OA_fft]),selectChans),2));
figure; scatter(x,y, 'filled')
% sign. relation including both age groups
[r,p] = corrcoef(x,y)
% partial correlation still significant after excluding binary age group
z = [x,y,[repmat(1,47,1); repmat(2,52,1)]];
[r,p] = partialcorr(z)

% resting slope - to task modulation
x = squeeze(nanmean(STSWD_summary.OneFslope.linear_win(logical(idx_YA_summary+idx_OA_summary), 1),2));
linFitRest = squeeze(nanmean(cat(3,linFit_2_30_EC, linFit_2_30_EO),3));
y = squeeze(nanmean(linFitRest(logical([idx_YA_fft+idx_OA_fft]),selectChans),2));
figure; scatter(x,y, 'filled')
% sign. relation including both age groups
[r,p] = corrcoef(x,y)
% partial correlation still significant after excluding binary age group
z = [x,y,[repmat(1,47,1); repmat(2,52,1)]];
[r,p] = partialcorr(z)

% resting slope - to task L1 behavior
x = squeeze(nanmean(STSWD_summary.HDDM_vt.driftEEG(logical(idx_YA_summary+idx_OA_summary), 1),2));
linFitRest = squeeze(nanmean(cat(3,linFit_2_30_EC, linFit_2_30_EO),3));
y = squeeze(nanmean(linFitRest(logical([idx_YA_fft+idx_OA_fft]),selectChans),2));
figure; scatter(x,y, 'filled')
% sign. relation including both age groups
[r,p] = corrcoef(x,y)
% partial correlation
z = [x,y,[repmat(1,47,1); repmat(2,52,1)]];
[r,p] = partialcorr(z)

% L1 slope - to L1 behavior
x = squeeze(nanmean(STSWD_summary.OneFslope.data(logical(idx_YA_summary+idx_OA_summary), 1),2));
y = squeeze(nanmean(STSWD_summary.HDDM_vt.driftEEG(logical(idx_YA_summary+idx_OA_summary), 1),2));
figure; scatter(x,y, 'filled')
[r,p] = corrcoef(x,y)
% partial correlation
z = [x,y,[repmat(1,47,1); repmat(2,52,1)]];
[r,p] = partialcorr(z)

% check task modulation vs. effect of eye opening
x = squeeze(nanmean(STSWD_summary.OneFslope.linear_win(logical(idx_YA_summary+idx_OA_summary), 1),2));
y = squeeze(nanmean(linFit_2_30_EO(logical([idx_YA_fft+idx_OA_fft]),selectChans)-linFit_2_30_EC(logical([idx_YA_fft+idx_OA_fft]),selectChans),2));
figure; scatter(x,y, 'filled')
% sign. relation including both age groups
[r,p] = corrcoef(x,y)
% partial correlation still significant after excluding binary age group
z = [x,y,[repmat(1,47,1); repmat(2,52,1)]];
[r,p] = partialcorr(z)
% This is not stable when dropping the winsorizing procedure (outliers
% would pose a problem for the correlation).

% relate to drift rates?
x = squeeze(nanmean(STSWD_summary.OneFslope.data(logical(idx_OA_summary), 1:4),2));
y = squeeze(nanmean(cat(2,squeeze(nanmean(STSWD_summary.HDDM_vt.driftEEG(logical(idx_OA_summary), 1:4),2)),...
    squeeze(nanmean(STSWD_summary.HDDM_vt.driftMRI(logical(idx_OA_summary), 1:4),2))),2));
figure; scatter(x,y, 'filled')
[r,p] = corrcoef(x,y)

 x = squeeze(nanmean(STSWD_summary.OneFslope.data(logical(idx_YA_summary), 1:4),2));
 y = squeeze(nanmean(cat(2,squeeze(nanmean(STSWD_summary.HDDM_vt.driftEEG(logical(idx_YA_summary), 1:4),2)),...
    squeeze(nanmean(STSWD_summary.HDDM_vt.driftMRI(logical(idx_YA_summary), 1:4),2))),2));
figure; scatter(x,y, 'filled')
[r,p] = corrcoef(x,y)

%% plot occipital aperiodic slopes side-by-side

YAslopes = load(fullfile(pn.data, 'C_SlopeFits_v3_YA.mat'), 'SlopeFits');
OAslopes = load(fullfile(pn.data, 'C_SlopeFits_v3_OA.mat'), 'SlopeFits');

YAslopes = squeeze(nanmean(YAslopes.SlopeFits.linFit_2_30(:,:,selectChans),3));
OAslopes = squeeze(nanmean(OAslopes.SlopeFits.linFit_2_30(:,:,selectChans),3));

colorm = [43/255 140/255 190/255; 230/255 75/255 50/255; 230/255 75/255 50/255];

h = figure('units','normalized','position',[.1 .1 .15 .2]);
hold on;
curMean = [nanmean(YAslopes,1),nanmean(OAslopes,1)];
curSE = [nanstd(YAslopes,[],1)./sqrt(size(YAslopes,1)),nanstd(OAslopes,[],1)./sqrt(size(OAslopes,1))];
[h1, herror] = barwitherr(curSE, curMean);
set(h1, 'FaceColor', [.6 .6 .6], 'EdgeColor', 'none','BarWidth', 0.8)
set(herror, 'LineWidth', 2)
ylabel('Posterior Slope'); xlabel('# of targets (YA/OA)');
ylim([-1.05, -.85])
title('Means +- SE')
set(findall(gcf,'-property','FontSize'),'FontSize',22)

figureName = 'd_1f_YAOA_extracted';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

[h,p] = ttest2(YAslopes(:,4), squeeze(nanmean(OAslopes(:,1:4),2)))

%% normalize by individual resting state slope fits

YAslopes = load(fullfile(pn.data, 'C_SlopeFits_v3_YA.mat'), 'SlopeFits');
OAslopes = load(fullfile(pn.data, 'C_SlopeFits_v3_OA.mat'), 'SlopeFits');

idx_YA_task = ismember(YAslopes.SlopeFits.IDs, commonYA);
idx_OA_task = ismember(OAslopes.SlopeFits.IDs, commonOA);

YAslopes = squeeze(nanmean(YAslopes.SlopeFits.linFit_2_30(:,:,selectChans),3));
OAslopes = squeeze(nanmean(OAslopes.SlopeFits.linFit_2_30(:,:,selectChans),3));

%linfit_rest = squeeze(nanmean(cat(3,linFit_2_30_EC,linFit_2_30_EO),3));
linfit_rest = linFit_2_30_EO;
linfit_rest_YA = squeeze(nanmean(linfit_rest(idx_YA_fft,58:60),2));
linfit_rest_OA = squeeze(nanmean(linfit_rest(idx_OA_fft,58:60),2));

YAslopes = YAslopes(idx_YA_task,:)-repmat(linfit_rest_YA,1,4);
OAslopes = OAslopes(idx_OA_task,:)-repmat(linfit_rest_OA,1,4);

colorm = [43/255 140/255 190/255; 230/255 75/255 50/255; 230/255 75/255 50/255];

h = figure('units','normalized','position',[.1 .1 .15 .2]);
hold on;
curMean = [nanmean(YAslopes,1),nanmean(OAslopes,1)];
curSE = [nanstd(YAslopes,[],1)./sqrt(size(YAslopes,1)),nanstd(OAslopes,[],1)./sqrt(size(OAslopes,1))];
[h1, herror] = barwitherr(curSE, curMean);
set(h1, 'FaceColor', [.6 .6 .6], 'EdgeColor', 'none','BarWidth', 0.8)
set(herror, 'LineWidth', 2)
ylabel('Posterior Slope'); xlabel('# of targets (YA/OA)');
%ylim([-1.05, -.85])
title('Means +- SE')
set(findall(gcf,'-property','FontSize'),'FontSize',22)
% 
% figureName = 'd_1f_YAOA_extracted';
% saveas(h, fullfile(pn.figures, figureName), 'epsc');
% saveas(h, fullfile(pn.figures, figureName), 'png');

%% perform a median split for the resting state data

grandavg_rest.powspctrm = squeeze(nanmean(cat(4,grandavg_EC.powspctrm, grandavg_EO.powspctrm),4));

cBrew = brewermap(4,'RdBu');
cBrew = flipud(cBrew);
h = figure('units','normalized','position',[.1 .1 .4 .2]); 
subplot(1,2,1); cla; hold on;

idxOA = ismember(STSWD_summary.IDs, commonOA);
drift = squeeze(nanmean(STSWD_summary.HDDM_vt.driftEEG(idxOA,1),2));
[driftIdx, sortIdx] = sort(drift, 'descend');
highdrift = sortIdx(1:ceil(numel(sortIdx)/2));
lowdrift = sortIdx(ceil(numel(sortIdx)/2)+1:numel(sortIdx));
idxChan = 58:60;
idx_OA_fft_num = find(idx_OA_fft);

curData = squeeze(nanmean(log10(grandavg_rest.powspctrm(idx_OA_fft_num(lowdrift),idxChan,:)),2));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l1 = shadedErrorBar(log10(grandavg_EO.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
curData = squeeze(nanmean(log10(grandavg_rest.powspctrm(idx_OA_fft_num(highdrift),idxChan,:)),2));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l4 = shadedErrorBar(log10(grandavg_EO.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
xlim([0.4 1.8]); ylim([-9.5 -7.5])
xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');
legend([l1.mainLine, l4.mainLine], {'Low drift', 'High drift'}, 'location', 'SouthEast'); legend('boxoff')
title('Resting state: EC/EO avg. [OA]')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
subplot(1,2,2); cla; hold on;

idxYA = ismember(STSWD_summary.IDs, commonYA);
drift = squeeze(nanmean(STSWD_summary.HDDM_vt.driftEEG(idxYA,1),2));
[driftIdx, sortIdx] = sort(drift, 'descend');
highdrift = sortIdx(1:ceil(numel(sortIdx)/2));
lowdrift = sortIdx(ceil(numel(sortIdx)/2)+1:numel(sortIdx));
idxChan = 58:60;
idx_YA_fft_num = find(idx_YA_fft);

curData = squeeze(nanmean(log10(grandavg_rest.powspctrm(idx_YA_fft_num(lowdrift),idxChan,:)),2));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l1 = shadedErrorBar(log10(grandavg_EO.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(nanmean(log10(grandavg_rest.powspctrm(idx_YA_fft(highdrift),idxChan,:)),2));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l4 = shadedErrorBar(log10(grandavg_EO.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
xlim([0.4 1.8]); ylim([-9.5 -7])
xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');
legend([l1.mainLine, l4.mainLine], {'Low drift', 'High drift'}, 'location', 'SouthEast'); legend('boxoff')
title('Resting state: EC/EO avg. [YA]')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% perform a median split for the task data

grandavg_rest.powspctrm = squeeze(nanmean(cat(4,grandavg_EC.powspctrm, grandavg_EO.powspctrm),4));

cBrew = brewermap(4,'RdBu');
cBrew = flipud(cBrew);
h = figure('units','normalized','position',[.1 .1 .4 .2]); 
subplot(1,2,1); cla; hold on;

idxOA = ismember(STSWD_summary.IDs, IDs(48:end));
drift = squeeze(nanmean(STSWD_summary.HDDM_vt.driftEEG(idxOA,1),2));
[driftIdx, sortIdx] = sort(drift, 'descend');
highdrift = sortIdx(1:ceil(numel(sortIdx)/2));
lowdrift = sortIdx(ceil(numel(sortIdx)/2)+1:numel(sortIdx));
idxChan = 58:60;
idx_OA_fft_num = 48:numel(IDs);

curData = squeeze(nanmean(nanmean(log10(MergedPow(idx_OA_fft_num(lowdrift),1,idxChan,:)),3),2));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
curData = squeeze(nanmean(nanmean(log10(MergedPow(idx_OA_fft_num(highdrift),1,idxChan,:)),3),2));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l4 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
xlim([0 1.9]); ylim([-10 -6.5])
xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');
legend([l1.mainLine, l4.mainLine], {'low drift', 'high drift'}, 'location', 'SouthEast'); legend('boxoff')
title('median split L1 OA')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

subplot(1,2,2); cla; hold on;

idxYA = ismember(STSWD_summary.IDs, commonYA(1:47));
drift = squeeze(nanmean(STSWD_summary.HDDM_vt.driftEEG(idxYA,1),2));
[driftIdx, sortIdx] = sort(drift, 'descend');
highdrift = sortIdx(1:ceil(numel(sortIdx)/2));
lowdrift = sortIdx(ceil(numel(sortIdx)/2)+1:numel(sortIdx));
idxChan = 58:60;
idx_YA_fft_num = 1:47;

curData = squeeze(nanmean(nanmean(log10(MergedPow(idx_YA_fft_num(lowdrift),1,idxChan,:)),3),2));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
curData = squeeze(nanmean(nanmean(log10(MergedPow(idx_YA_fft_num(highdrift),1,idxChan,:)),3),2));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l4 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
xlim([0 1.9]); ylim([-10 -6.5])
xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');
legend([l1.mainLine, l4.mainLine], {'low drift', 'high drift'}, 'location', 'SouthEast'); legend('boxoff')
title('median split L1 YA')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% check whether there is frontal 1/f rotation

% idx_chan = 1:24;
% 
% % shading presents within-subject standard errors
% % new value = old value ??? subject average + grand average
% 
% cBrew = brewermap(4,'RdBu');
% cBrew = flipud(cBrew);
% h = figure('units','normalized','position',[.1 .1 .1 .2]); hold on;
% condAvg = squeeze(nanmean(nanmean(log10(MergedPow(:,:,idx_chan,:)),3),2));
% curData = squeeze(nanmean(nanmean(log10(MergedPow(:,[1,4],idx_chan,:)),3),2));
% curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
% standError = nanstd(curData,1)./sqrt(size(curData,1));
% l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
%     'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
% % curData = squeeze(nanmean(log10(MergedPow(:,2,idx_chan,:)),3));
% % curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
% % standError = nanstd(curData,1)./sqrt(size(curData,1));
% % l2 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
% %     'lineprops', {'color', cBrew(3,:),'linewidth', 2}, 'patchSaturation', .25);
% curData = squeeze(nanmean(nanmean(log10(MergedPow(:,[2,3],idx_chan,:)),3),2));
% curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
% standError = nanstd(curData,1)./sqrt(size(curData,1));
% l3 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
%     'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
% xlim([0 1.9]); %ylim([-10 -6.5])
% xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');
% legend([l1.mainLine, l3.mainLine], {'1 Target', '3 Targets'}, 'location', 'SouthEast'); legend('boxoff')
% set(findall(gcf,'-property','FontSize'),'FontSize',18)
% 
% cBrew = brewermap(4,'RdBu');
% cBrew = flipud(cBrew);
% h = figure('units','normalized','position',[.1 .1 .1 .2]); hold on;
% curData = squeeze(nanmean(nanmean(log10(MergedPow(:,[1,4],idx_chan,:)),3),2))-squeeze(nanmean(nanmean(log10(MergedPow(:,[2,3],idx_chan,:)),3),2));
% standError = nanstd(curData,1)./sqrt(size(curData,1));
% l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
%     'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
% 
% cBrew = brewermap(4,'RdBu');
% cBrew = flipud(cBrew);
% h = figure('units','normalized','position',[.1 .1 .1 .2]); hold on;
% curData = squeeze(nanmean(nanmean(log10(MergedPow(:,[1],idx_chan,:)),3),2))-squeeze(nanmean(nanmean(log10(MergedPow(:,[1:4],idx_chan,:)),3),2));
% standError = nanstd(curData,1)./sqrt(size(curData,1));
% l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
%     'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
% curData = squeeze(nanmean(nanmean(log10(MergedPow(:,[2],idx_chan,:)),3),2))-squeeze(nanmean(nanmean(log10(MergedPow(:,[1:4],idx_chan,:)),3),2));
% standError = nanstd(curData,1)./sqrt(size(curData,1));
% l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
%     'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
% 
% curData = squeeze(nanmean(nanmean(log10(MergedPow(:,[3],idx_chan,:)),3),2))-squeeze(nanmean(nanmean(log10(MergedPow(:,[1:4],idx_chan,:)),3),2));
% standError = nanstd(curData,1)./sqrt(size(curData,1));
% l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
%     'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
% 
% 
% curData = squeeze(nanmean(nanmean(log10(MergedPow(:,[4],idx_chan,:)),3),2))-squeeze(nanmean(nanmean(log10(MergedPow(:,[1:4],idx_chan,:)),3),2));
% standError = nanstd(curData,1)./sqrt(size(curData,1));
% l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
%     'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
% 
% 
